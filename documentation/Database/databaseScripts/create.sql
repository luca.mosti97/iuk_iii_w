CREATE TABLE T_Company
(
    pk_Company INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    companyName VARCHAR(100),
    street VARCHAR(100),
    city VARCHAR(100),
    postalcode VARCHAR(100)
);

CREATE TABLE T_Visitor
(
    pk_Visitor INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    title VARCHAR(50),
    firstName VARCHAR(100),
    lastName VARCHAR(100),
    fk_company INTEGER REFERENCES T_Company ( pk_Company ),
    postalcode VARCHAR(100)
);

CREATE TABLE T_Employee
(
    pk_Employee INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    firstName VARCHAR(100),
    lastName VARCHAR(100),
    telephone VARCHAR(100)
);

CREATE TABLE T_Visit
(
    pk_Visit INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    fk_Visitor INTEGER REFERENCES T_Visitor ( pk_Visitor ),
    fk_Employee INTEGER REFERENCES T_Employee ( pk_Employee ),
    badge BOOLEAN,
    arrival TIMESTAMP,
    departure TIMESTAMP
);