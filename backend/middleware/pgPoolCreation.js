const {pool} = require("../db/db-config");
const dateTimeParser = require("./timeParsing");

//##########################################################################################################

/**
 * For adding something into the database
 *
 * @param sql       SQL INSERT INTO statement, requires: RETURNING xyz AS id
 * @param values    Values that are added into the DB, Array
 * @param res       Http Response
 * @param type      String, defines what was added
 * @returns {Promise<void>}
 */
const postRequestPool = async (sql, values, res, type) => {
    await pool.query(sql, values, (error, results) => {
        if (error) {
            //duplicate entry
            if (error.code === "23503") {
                if (type === "Visit") {
                    res.status(403).json({
                        message: "Visitor alread has an active Visit"
                    });
                    return;
                } else {
                    res.status(403).json({
                        message: type + " already exists"
                    });
                    return;
                }
            }
            //invalid syntax
            if (error.code === "22P02") {
                res.status(400).json({
                    message: "Invalid syntax"
                });
                return;
            }
            console.log(error)
            res.status(500).end();
            return;
        }
        if (!results.rowCount) {
            if (type === "Visit") {
                res.status(403).json({
                    message: "Visitor alread has an active Visit"
                });
            } else {
                res.status(403).json({
                    message: type + " already exists"
                });
            }
        } else {
            let returnedData = results.rows[0];
            res.status(201).json({
                message: "Successfully added a new " + type,
                id: returnedData.id
            });
        }
    });
}

/**
 * For updating something in the database
 *
 * @param sql       SQL UPDATE ... SET statement
 * @param values    Values that are added into the DB, Array
 * @param type      String, adds request "type" to a 404 error
 * @param res       Http Response
 * @returns {Promise<void>}
 */
const putRequestPool = async (sql, values, type, res) => {
    await pool.query(sql, values, (error, results) => {
        if (error) {
            //invalid syntax
            if (error.code === "22P02") {
                res.status(400).json({
                    message: "Invalid syntax"
                });
                return;
            }
            console.log(error);
            res.status(500).end();
            return;
        }

        if (!results.rowCount) {
            res.status(404).json({error: type + " not found"});
            return;
        }

        res.status(200).json({
            message: "Successfully updated " + type
        });
    });
}

/**
 * Requesting something with request parameters, such as an id
 *
 * @param sql           SQL SELECT statement
 * @param values        Parameters, as Array
 * @param type          String, adds request "type" to a 404 error
 * @param containsTime  Boolean, determines if TimeParser is needed
 * @param res           Http Response
 * @returns {Promise<void>}
 */
const getRequestPoolWithValues = async (sql, values, type, containsTime, res) => {
    await pool.query(sql, values, (error, results) => {
        if (error) {
            //invalid syntax
            if (error.code === "22P02") {
                res.status(400).json({
                    message: "Invalid syntax"
                });
                return;
            }
            console.log(error);
            res.status(500).end();
            return;
        }
        if (!results.rowCount) {
            res.status(404).json({error: type + " not found"});
            return;
        }
        let data = results.rows;
        if (containsTime) {
            dateTimeParser.parseDateTime(data)
        }
        res.status(200).json(data);
    });
}

/**
 * Requesting something without any parameters
 *
 * @param sql           SQL SELECT statement
 * @param type          String, adds request "type" to a 404 error
 * @param containsTime  Boolean, determines if TimeParser is needed
 * @param res           Http Response
 * @returns {Promise<void>}
 */
const getRequestPoolWithoutValues = async (sql, type, containsTime, res) => {
    await pool.query(sql, (error, results) => {
        if (error) {
            //invalid syntax
            if (error.code === "22P02") {
                res.status(400).json({
                    message: "Invalid syntax"
                });
                return;
            }
            console.log(error);
            res.status(500).end();
            return;
        }
        if (!results.rowCount) {
            res.status(404).json({error: type + " not found"});
            return;
        }
        let data = results.rows;

        if (containsTime) {
            dateTimeParser.parseDateTime(data)
        }
        res.status(200).json(data);
    });
}

/**
 * For the Update Method, when a Visitor is leaving
 *
 * @param sql1      SQL Statement, tests if Visit exists
 * @param sql2      SQL Statement, what should change, if Visit exists
 * @param values    Parameters, as Array, VisitID
 * @param res       Http Response
 * @returns {Promise<void>}
 */
const putRequestPoolTwoSQL = async (sql1, sql2, values, res) => {
    await pool.query(sql1, values, (error, results) => {
        if (error) {
            //invalid syntax
            if (error.code === "22P02") {
                res.status(400).json({
                    message: "Invalid syntax"
                });
                return;
            }
            console.log(error);
            res.status(500).end();
            return;
        }

        if (!results.rowCount) {
            res.status(404).json({error: "Visit not found"});
            return;
        }

        /**
         * If visit exists, try this SQL, if nothing is returned, the departure is already set, return a 403 - Forbidden in that case
         */
        pool.query(sql2, values, (error, results) => {
            if (error) {
                console.log(error);
                res.status(500).end();
                return;
            }

            if (!results.rowCount) {
                res.status(403).json({error: "Visitor already departed"});
                return;
            }
            res.status(201).json({
                message: "Successfully updated visit"
            });
        });
    });
}

//##########################################################################################################

module.exports = {
    postRequestPool,
    putRequestPool,
    getRequestPoolWithValues,
    getRequestPoolWithoutValues,
    putRequestPoolTwoSQL
}