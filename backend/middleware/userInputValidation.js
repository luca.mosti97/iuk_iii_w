/**
 * Validates User Input
 * @param fields    All Required Fields in the Data
 * @param req       Request with body
 * @param res       Response
 * @returns {Promise<*>}
 */
const validateInput = async (fields, req, res) => {
    let errors = [];
    let status;

    const values = await req.body;
    if (!values) {
        await res
            .status(400)
            .json({ errors: "Please provide the required data" });
        return;
    }

    for (let i = 0; i < fields.length; i++) {
        /**
         * Checks if Field is empty and if it is of the type boolean, because field "badge" might be false
         */
        if (!values[fields[i]] && typeof(values[fields[i]]) != "boolean") {
            status = 422;
            errors.push({ [fields[i]]: `${fields[i]} is required` });
        }
    }

    if (status) {
        await res.status(status).json({ errors });
        return;
    }
    return values;
}

/**
 * if at some point a validation for phonenumbers is needed, the following regex should do it
 *
 * const regexPhoneTest = /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/
 * check if phonenumber with if(fields[i] === "phonenumber") or similar
 * then simply invoke regexPhoneTest.test(valuesToTestAgainst), which returns a boolean value
 */

module.exports = {
    validateInput
};
