/**
 * For changing UTC Date to Local Time String Format
 * SHOULD work for dates around midnight, but needs further testing
 *
 * If JS Date Object is needed, the String works to create a new Date Object
 * localDepartureTime = formatTime(departureTime)
 * new Date(localDepartureTime)
 *
 * @param data  JSON Data, not empty
 * @returns {*} Changed JSON Data with different Time String
 */
const parseDateTime = (data) => {
    for (var i = 0; i < data.length; i++) {
        let arrivalTime = data[i]["arrival"];
        data[i]["arrival"] = formatTime(arrivalTime);

        if (data[i]["departure"]) {
            if (data[i]["departure"] !== null) {
                let departureTime = data[i]["departure"];
                data[i]["departure"] = formatTime(departureTime);
            }
        }
    }
    return data;
}

const formatTime = (date) => {
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    if (0 < month && month < 10) {
        month = "0" + month;
    }
    let day = date.getDate();
    if (0 < day && day < 10) {
        day = "0" + day;
    }
    let time = date.toLocaleTimeString();
    let timezone = date.toString().split(" ")[5];
    let newDate = year + '-' + month + '-' + day + " " + time + " " + timezone;
    return newDate;
}

module.exports = {
    parseDateTime
}