const LocalStrategy = require("passport-local").Strategy;
const bcrypt = require("bcryptjs");

module.exports = (passport, dbpool) => {
    passport.use(new LocalStrategy(
        function (username, password, cb) {
            dbpool.query("SELECT userid, roleid, username, password, role FROM V_Users WHERE user = $1", [username], (err, result) => {
                if (err) {
                    console.error("Error when selecting user on login " + err);
                    return cb(err);
                }
                if (result.rows.length > 0) {
                    const firstRow = result.rows[0];
                    bcrypt.compare(password, firstRow.password, function (err, res) {
                        if (res) {
                            cb(null, {id: firstRow.userid, username: firstRow.username, type: firstRow.role});
                        } else {
                            cb(null, false);
                        }
                    });
                } else {
                    cb(null, false);
                }
            });
        })
    );

    passport.serializeUser((user, done) => {
        done(null, user.id);
    });

    passport.deserializeUser((id, cb) => {
        dbpool.query("SELECT userid, username, type FROM users WHERE id = $1", [parseInt(id, 10)], (err, results) => {
            if (err) {
                console.error("Error when selecting user on session deserialize " + err);
                return cb(err);
            }
            cb(null, results.rows[0]);
        });
    });
}