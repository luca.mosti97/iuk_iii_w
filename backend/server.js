//Applikationsvariablen
require("dotenv").config();

//Session and Hashing
/*
const session = require("express-session");
const passport = require("passport");
*/
const express = require("express");
const app = express();
const port = process.env.PORT || 4000;

//Cross-origin
/*
const cors = require("cors");
app.use(cors());
*/

//Bodyparser
const bodyParser = require("body-parser");
app.use(bodyParser.json());

//Catch errors of parsing
app.use((error, req, res, next) => {
    if (error instanceof SyntaxError || error instanceof Error) {
        res.status(400).json({error: "error parsing data"});
    }
});

/**
 * Security best-practice https://expressjs.com/en/advanced/best-practice-security.html
 */
const helmet = require("helmet");
app.use(helmet());


//Session
/*
const maxAge = (60*60*1000) // 1 hour
app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false,
    cookie: {
        secure: false, //true, //sends only over HTTPS, set false during development
        httpOnly: true,
        path: '/',
        maxAge: maxAge,
        sameSite: false
    }
}));

const dbpool = require("./db/db-config");
require("./middleware/passport")(passport, dbpool);


app.use(passport.initialize());
app.use(passport.session());
*/

//Routes
const router = require("./routes/routes.js");
app.use('/api', router);

//Applikation auf Port starten
app.listen(port, () =>
    console.log(`listening on port ${port}`)
);


//Serve static files from the react app
const path = require("path");
app.use(express.static(path.join(__dirname, "../build")));

app.get('/*', function (req, res) {
    res.sendFile(path.join(__dirname, '../build', 'index.html'));
});


/*
const tls = require("tls");
//Information on how to generate key: https://nodejs.org/en/knowledge/cryptography/how-to-use-the-tls-module/
var options = {
    key: process.env.SERVER_KEY,
    cert: process.env.SERVER_CRT
};

tls
    .createServer(options, app)
    .listen(port);
*/
