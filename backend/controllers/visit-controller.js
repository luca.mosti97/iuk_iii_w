const validator = require("../middleware/userInputValidation");
const postgresPool = require("../middleware/pgPoolCreation");

//##########################################################################################################
//Changing or adding

/**
 * Muss noch angepasst werden, damit newVisitor aufgerufen wird, wenn noch nicht vorhanden
 *
 * @param req   Http POST Request, requires fields values as a JSON body
 * @param res   Http Response
 * @returns {Promise<void>}
 */
const addNewVisit = async (req, res) => {
    const type = "Visit";
    const fields = [
        "visitorid",
        "employeeid",
        "badge"
    ];

    let data = await validator.validateInput(fields, req, res);
    if (!data) {
        return;
    }

    let visitorId = Number(data.visitorid);
    let employeeId = Number(data.employeeid);
    let badge = data.badge;

    const sql = "INSERT INTO T_Visits (fk_Visitor, fk_Employee, badge, arrival) SELECT $1, $2, $3, now() WHERE NOT EXISTS(SELECT * FROM T_Visits WHERE fk_Visitor = $1 AND departure IS NULL) RETURNING pk_Visit AS id;";
    const values = [visitorId, employeeId, badge];

    await postgresPool.postRequestPool(sql, values, res, type);
}

/**
 * Update a visit, because visitor is departing
 *
 * @param req   Http POST Request, ID of Visit required in URL
 * @param res   Http Response
 * @returns {Promise<void>}
 */
const updateVisitVisitorLeaving = async (req, res) => {
    let visitId = Number(req.params.id);
    const values = [visitId];

    /**
     * Test if visit exists
     */
    const sqlTest = "SELECT * FROM T_Visits  WHERE pk_Visit = $1";

    /**
     * If visit exists, do this, if departure is NULL, nothing is returned
     * @type {string}
     */
    const sqlUpdate = "UPDATE T_Visits SET departure = now() WHERE pk_Visit = $1 AND departure IS NULL";

    await postgresPool.putRequestPoolTwoSQL(sqlTest, sqlUpdate, values, res);
}

/**
 * Reactivating a recently departed Visitor, adds a new Visit
 *
 * @param req   Http Request, requires visitid of recently departed Visit in the body
 * @param res   Http Response
 * @returns {Promise<void>}
 */
const reactivateVisitorRecentlyDeparted = async (req, res) => {
    const type = "Visit";
    const fields = [
        "visitid"
    ];

    let data = await validator.validateInput(fields, req, res);
    if (!data) {
        return;
    }

    let visitId = Number(data.visitid);

    const sql = "INSERT INTO T_Visits (fk_Visitor, fk_Employee, badge, arrival) SELECT fk_Visitor, fk_Employee, badge, now() FROM T_Visits WHERE pk_Visit = $1 AND NOT EXISTS(SELECT * FROM T_Visits WHERE fk_Visitor = (SELECT fk_Visitor FROM T_Visits WHERE pk_Visit = $1) AND departure IS NULL) RETURNING pk_Visit AS id;";
    const values = [visitId];

    await postgresPool.postRequestPool(sql, values, res, type);
}

/**
 * Update information of an active Visit, if details changed
 *
 * @param req   Http Request, requires visitid as param and details in the body
 * @param res   Http Response
 * @returns {Promise<void>}
 */
const updateVisitDetails = async (req, res) => {
    const visitId = Number(req.params.id);
    const type = "Active Visit";
    const fields = [
        "employeeid",
        "badge"
    ];

    let data = await validator.validateInput(fields, req, res);
    if (!data) {
        return;
    }

    let employeeid = Number(data.employeeid);
    let badge = data.badge;

    const sql = "UPDATE T_Visits SET fk_Employee = $2, badge = $3 WHERE pk_Visit = $1 AND departure IS NULL";
    const values = [visitId, employeeid, badge];

    await postgresPool.putRequestPool(sql, values, type, res);
}

//##########################################################################################################
//Retrieving values

/**
 * Get all Visits, even when departed
 *
 * @param req   Http Request
 * @param res   Http Response
 * @returns {Promise<void>}
 */
const getAllVisits = async (req, res) => {
    const type = "Visits";
    const sql = "SELECT * FROM V_VisitsCombined";

    await postgresPool.getRequestPoolWithoutValues(sql, type, true, res);
}

/**
 * Get all Visits, where no one has departed/active visitors
 *
 * @param req   Http Request
 * @param res   Http Response
 * @returns {Promise<void>}
 */
const getAllActiveVisits = async (req, res) => {
    const type = "Active Visits";
    const sql = "SELECT * FROM V_VisitsOverviewPage";

    await postgresPool.getRequestPoolWithoutValues(sql, type, true, res);
}

/**
 * Get one specifig visit, where visitor has not departed
 *
 * @param req   Http Request, requires ID of Visit in the URL
 * @param res   Http Response
 * @returns {Promise<void>}
 */
const getSpecificActiveVisit = async (req, res) => {
    const type = "Active Visit";
    let visitId = Number(req.params.id);

    const sql = "SELECT * FROM V_VisitsActiveForSinglePage WHERE visitid = $1";
    const values = [visitId];

    await postgresPool.getRequestPoolWithValues(sql, values, type, true, res);
}

/**
 * Get one specifig visit, where visitor has already departed
 *
 * @param req   Http Request, requires ID of Visit in the URL
 * @param res   Http Response
 * @returns {Promise<void>}
 */
const getSpecificDepartedVisit = async (req, res) => {
    const type = "Departed Visit";
    let visitId = Number(req.params.id);

    const sql = "SELECT * FROM V_VisitsDeparted WHERE visitid = $1";
    const values = [visitId];

    await postgresPool.getRequestPoolWithValues(sql, values, type, true, res);
}

/**
 * Get all Visits with a departure of today
 *
 * @param req   Http Request
 * @param res   Http Response
 * @returns {Promise<void>}
 */
const getLastDepartedVisits = async (req, res) => {
    const type = "Departed Visit";
    const sql = "SELECT * FROM V_VisitsRecentlyDeparted ORDER BY departure DESC";

    await postgresPool.getRequestPoolWithoutValues(sql, type, true, res);
}

//##########################################################################################################

module.exports = {
    addNewVisit,
    updateVisitVisitorLeaving,
    reactivateVisitorRecentlyDeparted,
    updateVisitDetails,
    getAllVisits,
    getAllActiveVisits,
    getSpecificActiveVisit,
    getSpecificDepartedVisit,
    getLastDepartedVisits
}