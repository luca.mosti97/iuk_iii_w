const postgresPool = require("../middleware/pgPoolCreation");

//##########################################################################################################

const getAllCountries = async (req, res) => {
    const type = "Country";
    const sql = "SELECT * FROM V_Countries";

    await postgresPool.getRequestPoolWithoutValues(sql, type, false, res);
}

//##########################################################################################################

module.exports = {
    getAllCountries
}
