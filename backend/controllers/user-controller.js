const postgresPool = require("../middleware/pgPoolCreation");

//##########################################################################################################

const getAllUsers = async (req, res) => {
    const type = "Country";
    const sql = "SELECT * FROM V_Users";

    await postgresPool.getRequestPoolWithoutValues(sql, type, false, res);
}

//##########################################################################################################

module.exports = {
    getAllUsers
}
