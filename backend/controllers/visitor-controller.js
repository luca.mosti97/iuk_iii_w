const validator = require("../middleware/userInputValidation");
const postgresPool = require("../middleware/pgPoolCreation");

//##########################################################################################################

/**
 * Adds a new Visitor
 *
 * @param req   Http Request, fields required in body
 * @param res   Http Response
 * @returns {Promise<void>}
 */
const addNewVisitor = async (req, res) => {
    const type = "Visitor";
    const fields = [
        "title",
        "firstname",
        "lastname",
        "companyid"
    ]

    let data = await validator.validateInput(fields, req, res);
    if (!data) {
        return;
    }

    let title = String(data.title);
    let firstName = String(data.firstname);
    let lastName = String(data.lastname);
    let companyId = Number(data.companyid);
    let mobileNumber = null;
    if (data.mobilenumber) {
        mobileNumber = String(data.mobilenumber);
    }

    const sql = "INSERT INTO T_Visitors (title, firstName, lastName, fk_company, telephone) SELECT $1, $2, $3, $4, $5 WHERE NOT EXISTS(SELECT * FROM T_Visitors WHERE firstName = CAST($2 AS VARCHAR) AND lastName = CAST($3 AS VARCHAR) AND fk_company = $4) RETURNING pk_Visitor AS id;";
    const values = [title, firstName, lastName, companyId, mobileNumber];

    await postgresPool.postRequestPool(sql, values, res, type);
}

/**
 * Returns all Visitors
 *
 * @param req   Http Request
 * @param res   Http Response
 * @returns {Promise<void>}
 */
const getAllVisitors = async (req, res) => {
    const type = "Visitor";
    const sql = "SELECT * FROM V_Visitors";

    await postgresPool.getRequestPoolWithoutValues(sql, type, false, res);
}

/**
 * Returns all Visitors, method for the creation of Selects in Frontend
 *
 * @param req   Http GET Request
 * @param res   Http Response
 * @returns {Promise<void>}
 */
const getAllVisitorsSelect = async (req, res) => {
    const type = "Visitor";
    const sql = "SELECT * FROM V_VisitorsForSelect";

    await postgresPool.getRequestPoolWithoutValues(sql, type, false, res);
}

/**
 * Returns specific Visitor
 *
 * @param req   Http Request, id required as parameter in URL
 * @param res   Http Response
 * @returns {Promise<void>}
 */
const getSpecificVisitor = async (req, res) => {
    const type = "Visitor";
    let visitorId = Number(req.params.id);

    const sql = "SELECT * FROM V_Visitors WHERE visitorid = $1";
    const values = [visitorId];

    await postgresPool.getRequestPoolWithValues(sql, values, type, false, res);
}

//##########################################################################################################

module.exports = {
    addNewVisitor,
    getAllVisitors,
    getAllVisitorsSelect,
    getSpecificVisitor
}
