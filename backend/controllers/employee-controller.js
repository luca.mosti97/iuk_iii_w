const validator = require("../middleware/userInputValidation");
const postgresPool = require("../middleware/pgPoolCreation");

//##########################################################################################################
//adding or changing data

/**
 * Adds a new Employee
 *
 * @param req   Http Request, requires "fields" in request body
 * @param res   Http Response
 * @returns {Promise<void>}
 */
const addEmployee = async (req, res) => {
    const type = "Employee";
    const fields = [
        "firstname",
        "lastname",
        "phonenumber"
    ];

    let data = await validator.validateInput(fields, req, res);
    if (!data) {
        return;
    }

    let firstName = String(data.firstname);
    let lastName = String(data.lastname);
    let phoneNumber = String(data.phonenumber);

    const sql = "INSERT INTO T_Employees (firstName, lastName, telephone) VALUES ($1, $2, $3) RETURNING pk_Employee AS id"
    const values = [firstName, lastName, phoneNumber];

    await postgresPool.postRequestPool(sql, values, res, type);
}

/**
 * Update information of an Employee, if details changed
 *
 * @param req   Http Request, requires employeeid as param and "fields" in request body
 * @param res   Http Response
 * @returns {Promise<void>}
 */
const updateEmployeeDetails = async (req, res) => {
    const employeeid = Number(req.params.id);
    const type = "Employee";
    const fields = [
        "firstname",
        "lastname",
        "phonenumber"
    ];

    let data = await validator.validateInput(fields, req, res);
    if (!data) {
        return;
    }

    let firstName = String(data.firstname);
    let lastName = String(data.lastname);
    let phoneNumber = String(data.phonenumber);

    const sql = "UPDATE T_Employees SET firstName = $2, lastName = $3, telephone = $4 WHERE pk_Employee = $1";
    const values = [employeeid, firstName, lastName, phoneNumber];

    await postgresPool.putRequestPool(sql, values, type, res);
}

//##########################################################################################################
//retrieving data

/**
 * Returns all Employees
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
const getEmployees = async (req, res) => {
    const type = "Employee";
    const sql = "SELECT * FROM V_Employees";

    await postgresPool.getRequestPoolWithoutValues(sql, type, false, res);
}

/**
 * Returns one specific Employee
 *
 * @param req   Http Request, requires employeeid as parameter
 * @param res
 * @returns {Promise<void>}
 */
const getSpecificEmployee = async (req, res) => {
    const type = "Employee";
    let employeeId = Number(req.params.id);

    const sql = "SELECT * FROM V_Employees WHERE employeeid = $1";
    const values = [employeeId];

    await postgresPool.getRequestPoolWithValues(sql, values, type, false, res);
}

//##########################################################################################################

module.exports = {
    addEmployee,
    updateEmployeeDetails,
    getEmployees,
    getSpecificEmployee
}