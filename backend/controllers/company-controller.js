const validator = require("../middleware/userInputValidation");
const postgresPool = require("../middleware/pgPoolCreation");

//##########################################################################################################

/**
 * Adds a new company
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
const addNewCompany = async (req, res) => {
    const type = "Company";
    const fields = [
        "company",
        "street",
        "city",
        "postalcode",
        "countryid"
    ]

    let data = await validator.validateInput(fields, req, res);
    if (!data) {
        return;
    }

    let companyName = String(data.company);
    let street = String(data.street);
    let city = String(data.city);
    let postalCode = String(data.postalcode);
    let countryid = String(data.countryid);

    const sql = "INSERT INTO T_Companies (companyName, street, city, postalcode, fk_Country) VALUES ($1, $2, $3, $4, $5) RETURNING pk_Company AS id";
    const values = [companyName, street, city, postalCode, countryid];

    await postgresPool.postRequestPool(sql, values, res, type);
}

/**
 * Returns all companies
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
const getAllCompanies = async (req, res) => {
    const type = "Company";
    const sql = "SELECT * FROM V_Companies";

    await postgresPool.getRequestPoolWithoutValues(sql, type, false, res);
};

/**
 * Returns a specific company
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
const getSpecificCompany = async (req, res) => {
    const type = "Company";
    let companyId = Number(req.params.id);

    const sql = "SELECT * FROM V_Companies WHERE companyid = $1";
    const values = [companyId];

    await postgresPool.getRequestPoolWithValues(sql, values, type, false, res);
}

//##########################################################################################################

module.exports = {
    addNewCompany,
    getAllCompanies,
    getSpecificCompany
}
