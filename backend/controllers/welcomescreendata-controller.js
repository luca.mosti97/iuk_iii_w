const validator = require("../middleware/userInputValidation");
const postgresPool = require("../middleware/pgPoolCreation");

//##########################################################################################################

/**
 * This method is to set the data for the welcome screen, it can be easily expanded, if a param ID is added
 *
 * @param req   Http Request, requires name in body, can have company in body
 * @param res   Http Response
 * @returns {Promise<void>}
 */
const setWelcomeScreenData = async (req, res) => {
    const type = "Welcome Data";
    const fields = [
        "name"
    ];

    let data = await validator.validateInput(fields, req, res);
    if (!data) {
        return;
    }

    let id = 1;
    let name = String(data.name);
    let company = "";
    if (data.company) {
        company = String(data.company);
    }

    const sql = "UPDATE T_WelcomeData SET name = $2, company = $3 WHERE pk_WelcomeData = $1";
    const values = [id, name, company];

    await postgresPool.putRequestPool(sql, values, type, res);
}

/**
 * Method for setting default empty values, can be expanded as well
 *
 * @param req   Http Request, does not require anything at the moment
 * @param res   Http Response
 * @returns {Promise<void>}
 */
const setDefaultWelcomeScreenData = async (req, res) => {
    const type = "Welcome Data";

    let id = 1;
    let name = "";
    let company = "";

    const sql = "UPDATE T_WelcomeData SET name = $2, company = $3 WHERE pk_WelcomeData = $1";
    const values = [id, name, company];

    await postgresPool.putRequestPool(sql, values, type, res);
}

/**
 * Method for retrieving the data
 *
 * @param req   Http Request
 * @param res   Http Response
 * @returns {Promise<void>}
 */
const getWelcomeScreenData = async (req, res) => {
    const type = "Welcome Data";

    const sql = "SELECT * FROM V_WelcomeData";

    await postgresPool.getRequestPoolWithoutValues(sql, type, false, res);
}

//##########################################################################################################

module.exports = {
    setWelcomeScreenData,
    setDefaultWelcomeScreenData,
    getWelcomeScreenData
}