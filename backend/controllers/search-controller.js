const postgresPool = require("../middleware/pgPoolCreation");
//const validator = require("../middleware/userInputValidation");

const visitorController = require("./visitor-controller");

//##########################################################################################################
//for user searching

/**
 * Search Method, searches for a Visitor with his first- and lastname
 * LIKE is not case-sensitive, change to ILIKE if  needed
 *
 * @param req       Query parameter, has to include either firstname or lastname, else all Visitors are returned
 * @param res       Http Response
 * @returns {Promise<void>}
 */
const searchForVisitor = async (req, res) => {
    let type = "Visitor";
    let searchData = req.query;

    if (!searchData.firstname && !searchData.lastname) {
        await visitorController.getAllVisitors(req, res);
        //Oder soll ein Error zurückgegeben werden?
        //res.status(400).json({error: "Please provide specific searchterms"});
        return;
    }

    let firstname = "%" + String(searchData.firstname) + "%";
    let lastname = "%" + String(searchData.lastname) + "%";

    const sql = "SELECT * FROM V_Visitors WHERE visitorfirstname LIKE $1 AND visitorlastname LIKE $2";
    const values = [firstname, lastname];

    await postgresPool.getRequestPoolWithValues(sql, values, type, false, res);
}

//##########################################################################################################
//for reports

/**
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
const reportMethod = async (req, res) => {
    let type = "Data";
    let searchValues = [];
    let sql;

    /**
     * request body
     */
    let searchData = req.query;
    let visitorid = searchData.visitorid;
    let companyid = searchData.companyid;
    let date1 = searchData.arrivaldate;
    let date2 = searchData.departuredate;

    /**
     * for parsing the dates and adding 1 day to departure Date, to fully include the set date
     */
    if (date1) {
        date1 = new Date(date1);
    }
    if (date2) {
        date2 = new Date(date2);
        date2.setDate(date2.getDate()+1);
    }

    /**
     * if the dates are set incorrectly
     */
    if (date1 && date2 && date2 < date1) {
        res.status(422).json({error: "Departure date is before Arrival date"});
        return;
    }

    /**
     * company is already tied in with visitor, so it's not needed if visitor is set
     */
    if (visitorid && companyid) {
        companyid = null;
    }

    /**
     * visitorid is given, methods for all combinations with date
     */
    if (visitorid && !companyid && date1 && date2) {
        sql = "SELECT * FROM V_VisitsDeparted WHERE visitorid = $1 AND arrival >= $2 AND departure > $2 AND arrival < $3 AND departure <= $3";
        searchValues = [visitorid, date1, date2];
    }
    if (visitorid && !companyid && date1 && !date2) {
        sql = "SELECT * FROM V_VisitsDeparted WHERE visitorid = $1 AND arrival >= $2 AND departure > $2";
        searchValues = [visitorid, date1];
    }
    if (visitorid && !companyid && !date1 && date2) {
        sql = "SELECT * FROM V_VisitsDeparted WHERE visitorid = $1 AND arrival < $2 AND departure <= $2";
        searchValues = [visitorid, date2];
    }
    if (visitorid && !companyid && !date1 && !date2) {
        sql = "SELECT * FROM V_VisitsDeparted WHERE visitorid = $1";
        searchValues = [visitorid];
    }

    /**
     * companyid is given, methods for all combinations with date
     */
    if (!visitorid && companyid && date1 && date2) {
        sql = "SELECT * FROM V_VisitsDeparted WHERE companyid = $1 AND arrival >= $2 AND departure > $2 AND arrival < $3 AND departure <= $3";
        searchValues = [companyid, date1, date2];
    }
    if (!visitorid && companyid && date1 && !date2) {
        sql = "SELECT * FROM V_VisitsDeparted WHERE companyid = $1 AND arrival >= $2 AND departure > $2";
        searchValues = [companyid, date1];
    }
    if (!visitorid && companyid && !date1 && date2) {
        sql = "SELECT * FROM V_VisitsDeparted WHERE companyid = $1 AND arrival < $2 AND departure <= $2";
        searchValues = [companyid, date2];
    }
    if (!visitorid && companyid && !date1 && !date2) {
        sql = "SELECT * FROM V_VisitsDeparted WHERE companyid = $1";
        searchValues = [companyid];
    }

    /**
     * neither visitorid nor companyid are given, only dates
     */
    if (!visitorid && !companyid && date1 && date2) {
        sql = "SELECT * FROM V_VisitsDeparted WHERE arrival >= $1 AND departure > $1 AND arrival < $2 AND departure <= $2";
        searchValues = [date1, date2];
    }
    if (!visitorid && !companyid && date1 && !date2) {
        sql = "SELECT * FROM V_VisitsDeparted WHERE arrival >= $1 AND departure > $1";
        searchValues = [date1];
    }
    if (!visitorid && !companyid && !date1 && date2) {
        sql = "SELECT * FROM V_VisitsDeparted WHERE arrival < $1 AND departure <= $1";
        searchValues = [date2];
    }

    /**
     * if nothing is given
     */
    if (!visitorid && !companyid && !date1 && !date2) {
        res.status(403).json({error: "Must include search terms"});
        return;
    }

    await postgresPool.getRequestPoolWithValues(sql, searchValues, type, true, res);
}

//##########################################################################################################
//experimental

/**
 * Search Method, searches for a given term in visitors first and lastname, aswell as the company
 * LIKE is not case-sensitive, change to ILIKE if  needed
 *
 * @param req   Http Request, requires a query with the name searchterm
 * @param res   Http Response
 * @returns {Promise<void>}
 */
const searchForVisitorOrCompany = async (req, res) => {
    let type = "Visitor";
    let searchData = req.query.searchterm;

    if (!searchData) {
        await visitorController.getAllVisitors(req, res);
        //Oder soll ein Error zurückgegeben werden?
        //res.status(400).json({error: "Please provide specific searchterms"});
        return;
    }
    let searchFor = "%" + searchData + "%";

    //const sql = "SELECT DISTINCT ON (visitorid) visitorid, companyid, title, visitorfirstname, visitorlastname, company, mobilenumber FROM V_Visitors WHERE visitorfirstname ILIKE $1 OR visitorlastname ILIKE $1 OR company ILIKE $1";
    const sql = "SELECT * FROM V_Visitors WHERE visitorfirstname LIKE $1 OR visitorlastname LIKE $1 OR company LIKE $1";
    const values = [searchFor];

    await postgresPool.getRequestPoolWithValues(sql, values, type, false, res);
}

/**
 * Returns all Visits where the arrival is between two Dates
 *
 * @param req   Http Request, requires two Dates in the body
 * @param res   Http Response
 * @returns {Promise<void>}
 */
/*
const getSpecificTimeSlot = async (req, res) => {
    const type = "Visit";
    const fields = [
        "dateone",
        "datetwo"
    ]
    let daten = await validator.validateInput(fields, req, res);
    if (!daten) {
        return;
    }

    let datum1 = new Date(daten.dateone);
    let datum2 = new Date(daten.datetwo);

    const sql = "SELECT * FROM V_VisitsCombined WHERE arrival BETWEEN $1 AND $2 ORDER BY arrival";
    const values = [datum1, datum2];

    await postgresPool.getRequestPoolWithValues(sql, values, type, true, res);
}
 */

/**
 * Returns all Visits of one specific Visitor
 * @param req   Http Request, requires Visitor ID in the URL
 * @param res   Http Response
 * @returns {Promise<void>}
 */
const getAllVisitsOfVisitor = async (req, res) => {
    const type = "Visit";
    let visitorid = Number(req.params.id);

    const sql = "SELECT * FROM V_VisitsCombined WHERE visitorid = $1 ORDER BY arrival";
    const values = [visitorid];

    await postgresPool.getRequestPoolWithValues(sql, values, type, true, res);
}

//##########################################################################################################

module.exports = {
    searchForVisitor,
    reportMethod,
    searchForVisitorOrCompany,
    //getSpecificTimeSlot,
    getAllVisitsOfVisitor
}