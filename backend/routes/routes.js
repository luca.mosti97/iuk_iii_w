const express = require("express");
const router = express.Router();

const visitorController = require("../controllers/visitor-controller");
const employeeController = require("../controllers/employee-controller");
const companyController = require("../controllers/company-controller");
const visitController = require("../controllers/visit-controller");
const searchController = require("../controllers/search-controller");
const countryController = require("../controllers/country-controller");
const userController = require("../controllers/user-controller");
const welcomeScreenController = require("../controllers/welcomescreendata-controller");

//not yet implemented
//const passport = require("passport")
//const requireAuthentication = require("../middleware/authentication");

/**
 * Limits the parameters, that can be added as :id
 * @type {string}   Regular Expression as a String
 */
const regexParamForId = "([1-9]{1}[0-9]*)";

router
    /**
     * there should be authentication from here on
     */
    /*
    .post("/login", passport.authenticate("local"))
    .all("*", requireAuthentication.requiresLogin)
     */
    /**
     * reception tasks
     */
    .post("/newVisit", visitController.addNewVisit)
    .get("/allEmployees", employeeController.getEmployees)
    .get("/allCompanies", companyController.getAllCompanies)
    .get("/allVisitors", visitorController.getAllVisitors)
    .get("/allVisitorsSelect", visitorController.getAllVisitorsSelect)

    .post("/newVisitor", visitorController.addNewVisitor)
    .get("/allCountries", countryController.getAllCountries)

    .get("/activeVisits", visitController.getAllActiveVisits)
    .get("/activeVisits/:id"+regexParamForId, visitController.getSpecificActiveVisit)
    .get("/lastDeparted", visitController.getLastDepartedVisits)
    .get("/departedVisit/:id"+regexParamForId, visitController.getSpecificDepartedVisit)
    .put("/visit/:id"+regexParamForId, visitController.updateVisitVisitorLeaving)
    .put("/changeVisit/:id"+regexParamForId, visitController.updateVisitDetails)

    .post("/newCompany", companyController.addNewCompany)

    .post("/reactivateVisit", visitController.reactivateVisitorRecentlyDeparted)

    /**
     * WelcomeScreen
     */
    .put("/welcomeData", welcomeScreenController.setWelcomeScreenData)
    .put("/defaultWelcomeData", welcomeScreenController.setDefaultWelcomeScreenData)
    .get("/welcomeData", welcomeScreenController.getWelcomeScreenData)

    /**
     * Employee relating tasks
     */
    .post("/newEmployee", employeeController.addEmployee)
    .get("/employee/:id"+regexParamForId, employeeController.getSpecificEmployee)
    .put("/employee/:id"+regexParamForId, employeeController.updateEmployeeDetails)

    /**
     * reports
     */
    .get("/reports", searchController.reportMethod)

    //search methods, need to be revised
    .get("/searchForVisitor", searchController.searchForVisitor)
    .get("/searchForVisitorOrCompany", searchController.searchForVisitorOrCompany)
    //.get("/specificTime", searchController.getSpecificTimeSlot)
    .get("/visitor/:id"+regexParamForId, visitorController.getSpecificVisitor)

    /**
     * for testing purposes
     */
    .get("/allVisits", visitController.getAllVisits)
    .get("/allUsers", userController.getAllUsers)
    .get("/company/:id"+regexParamForId, companyController.getSpecificCompany)
    .get("/visitsOfVisitor/:id"+regexParamForId, searchController.getAllVisitsOfVisitor)

    /**
     * for error handling, if path is not found
     */
    /*
    .use(function (req, res, next) {
        if (!req.route) {
            if (req.originalUrl) {
                //delete query parameters to only return the original path
                const path = req.originalUrl.replace(/\?.*$/, '');
                res.status(404).json({
                    url: path,
                    error: "Path to " + path + " not found. Please try a different Path."
                })
                return;
            }
            res.status(404).json({error: "Path not found"})
            return;
        }
        next();
    })
     */

module.exports = router;
