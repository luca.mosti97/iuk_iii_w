require("dotenv").config();

const {Pool} = require("pg");
const isProduction = process.env.NODE_ENV === 'production'

const connectionString = `postgresql://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_DATABASE}`;

const pool = new Pool({
    connectionString: isProduction ? process.env.DATABASE_URL :
        connectionString,
    ssl: isProduction ? { rejectUnauthorized: false } : null,
    client_encoding: 'utf8'
});

//general error handling on pool
pool.on("error", (err, client) => {
    console.error("Error: ", err);
    process.exit(-1)
});

module.exports = {
    pool
};
