CREATE TABLE T_Countries
(
    pk_Country INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    name VARCHAR(80) NOT NULL,
    name_german VARCHAR(80) NOT NULL,
    iso CHAR(2) NOT NULL,
    iso3 CHAR(3) NOT NULL
);

CREATE TABLE T_Companies
(
    pk_Company INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    companyName VARCHAR(50) NOT NULL,
    street VARCHAR(50) NOT NULL,
    city VARCHAR(50) NOT NULL,
    postalcode VARCHAR(15) NOT NULL,
    fk_Country INTEGER REFERENCES T_Countries ( pk_Country )
);

CREATE TABLE T_Visitors
(
    pk_Visitor INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    title VARCHAR(50) NOT NULL,
    firstName VARCHAR(50) NOT NULL,
    lastName VARCHAR(50) NOT NULL ,
    fk_company INTEGER REFERENCES T_Companies ( pk_Company ) NOT NULL,
    telephone VARCHAR(20) NULL,
    UNIQUE (firstname, lastname, fk_Company)
);

CREATE TABLE T_Employees
(
    pk_Employee INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    firstName VARCHAR(50) NOT NULL,
    lastName VARCHAR(50) NOT NULL ,
    telephone VARCHAR(20) NOT NULL
);

CREATE TABLE T_Visits
(
    pk_Visit INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    fk_Visitor INTEGER REFERENCES T_Visitors ( pk_Visitor ) NOT NULL,
    fk_Employee INTEGER REFERENCES T_Employees ( pk_Employee ) NOT NULL,
    badge BOOLEAN NOT NULL,
    arrival TIMESTAMP WITH TIME ZONE NOT NULL,
    departure TIMESTAMP WITH TIME ZONE NULL,
    CONSTRAINT arrival_before_departure CHECK (arrival < departure),
    CONSTRAINT arrival_not_in_future CHECK (arrival <= now()),
    CONSTRAINT departure_not_in_future CHECK (departure <= now())
);
CREATE UNIQUE INDEX only_one_active_visit ON T_Visits (fk_Visitor) WHERE departure IS NULL;

CREATE TABLE T_Roles
(
    pk_Role INTEGER,
    role VARCHAR(14) NOT NULL,
    PRIMARY KEY (pk_Role)
);

CREATE TABLE T_Users
(
    pk_User INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    username VARCHAR(40) NOT NULL,
    password VARCHAR(100) NOT NULL,
    fk_role INTEGER REFERENCES T_Roles(pk_Role) NOT NULL
);

CREATE TABLE T_WelcomeData
(
    pk_WelcomeData INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    name VARCHAR(80),
    company VARCHAR(50)
);
