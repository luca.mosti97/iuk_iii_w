CREATE VIEW V_VisitsCombined AS
    SELECT
           T_Visits.pk_Visit AS visitid, T_Visits.fk_Visitor AS visitorid, T_Companies.pk_Company AS companyid,
           T_Visitors.title, T_Visitors.firstname AS visitorfirstname, T_Visitors.lastname AS visitorlastname,
           T_Companies.Companyname AS company, T_Visitors.telephone AS visitormobilenumber,
           T_Employees.firstname AS employeefirstname, T_Employees.lastname AS employeelastname,
           T_Visits.badge, T_Visits.arrival, T_Visits.departure
    FROM T_Visitors
        INNER JOIN T_Visits
            ON T_Visitors.pk_Visitor = T_Visits.fk_Visitor
        LEFT OUTER JOIN T_Companies
            ON T_Visitors.fk_Company = T_Companies.pk_Company
        INNER JOIN T_Employees
            ON T_Visits.fk_Employee = T_Employees.pk_Employee
    ORDER BY visitid
;

CREATE VIEW V_VisitsOverviewPage AS
    SELECT
           T_Visits.pk_Visit AS visitid, T_Visits.fk_Visitor AS visitorid, T_Companies.pk_Company AS companyid,
           T_Visitors.firstname AS visitorfirstname, T_Visitors.lastname AS visitorlastname, T_Companies.Companyname AS company,
           T_Employees.firstname AS employeefirstname, T_Employees.lastname AS employeelastname,
           T_Visits.arrival, (now()-T_Visits.arrival) AS timeelapsed
    FROM T_Visitors
        INNER JOIN T_Visits
            ON T_Visitors.pk_Visitor = T_Visits.fk_Visitor
        LEFT OUTER JOIN T_Companies
            ON T_Visitors.fk_Company = T_Companies.pk_Company
        INNER JOIN T_Employees
            ON T_Visits.fk_Employee = T_Employees.pk_Employee
    WHERE
          T_Visits.departure IS NULL
    ORDER BY arrival DESC
;

CREATE VIEW V_VisitsActiveForSinglePage AS
    SELECT
           T_Visits.pk_Visit AS visitid, T_Visits.fk_Visitor AS visitorid, T_Companies.pk_Company AS companyid,
           T_Visitors.title, T_Visitors.firstname AS visitorfirstname, T_Visitors.lastname AS visitorlastname,
           T_Companies.Companyname AS company, T_Visitors.telephone AS mobilenumber,
           T_Employees.pk_Employee AS employeeid,T_Employees.firstname AS employeefirstname, T_Employees.lastname AS employeelastname,
           T_Visits.badge, T_Visits.arrival, (now()-T_Visits.arrival) AS timeelapsed
    FROM T_Visitors
        INNER JOIN T_Visits
            ON T_Visitors.pk_Visitor = T_Visits.fk_Visitor
        LEFT OUTER JOIN T_Companies
            ON T_Visitors.fk_Company = T_Companies.pk_Company
        INNER JOIN T_Employees
            ON T_Visits.fk_Employee = T_Employees.pk_Employee
    WHERE
        T_Visits.departure IS NULL
    ORDER BY arrival DESC
;

--shows recently departed visitors, if they do not have an active visit
CREATE VIEW V_VisitsRecentlyDeparted AS
    SELECT
        DISTINCT ON (T_Visits.fk_Visitor)
           T_Visits.pk_Visit AS visitid, T_Visits.fk_Visitor AS visitorid, T_Companies.pk_Company AS companyid,
           T_Visitors.title, T_Visitors.firstname AS visitorfirstname, T_Visitors.lastname AS visitorlastname,
           T_Companies.Companyname AS company,T_Visitors.telephone AS mobilenumber,
           T_Employees.pk_Employee AS employeeid,T_Employees.firstname AS employeefirstname, T_Employees.lastname AS employeelastname,
           T_Visits.badge, T_Visits.arrival, T_Visits.departure
    FROM T_Visitors
        INNER JOIN T_Visits
            ON T_Visitors.pk_Visitor = T_Visits.fk_Visitor
        LEFT OUTER JOIN T_Companies
            ON T_Visitors.fk_Company = T_Companies.pk_Company
        INNER JOIN T_Employees
            ON T_Visits.fk_Employee = T_Employees.pk_Employee
    WHERE
          T_Visits.departure >= now()::date
      AND NOT EXISTS(SELECT * FROM V_VisitsOverviewPage WHERE V_VisitsOverviewPage.visitorid = T_Visits.fk_Visitor)
    ORDER BY T_Visits.fk_Visitor, T_Visits.departure DESC
;

CREATE VIEW V_VisitsDeparted AS
    SELECT
           T_Visits.pk_Visit AS visitid, T_Visits.fk_Visitor AS visitorid, T_Companies.pk_Company AS companyid,
           T_Visitors.title, T_Visitors.firstname AS visitorfirstname, T_Visitors.lastname AS visitorlastname,
           T_Companies.Companyname AS company,T_Visitors.telephone AS mobilenumber,
           T_Employees.firstname AS employeefirstname, T_Employees.lastname AS employeelastname,
           T_Visits.badge, T_Visits.arrival, T_Visits.departure
    FROM T_Visitors
        INNER JOIN T_Visits
            ON T_Visitors.pk_Visitor = T_Visits.fk_Visitor
        LEFT OUTER JOIN T_Companies
            ON T_Visitors.fk_Company = T_Companies.pk_Company
        INNER JOIN T_Employees
            ON T_Visits.fk_Employee = T_Employees.pk_Employee
    WHERE
          T_Visits.departure IS NOT NULL
    ORDER BY departure DESC
;

/* ########################################################################################################### */

CREATE VIEW V_Companies AS
    SELECT
           T_Companies.pk_Company AS companyid, T_Companies.fk_Country AS countryid,
           T_Companies.companyName AS company, T_Companies.street, T_Companies.city, T_Companies.postalcode,
           T_Countries.iso AS countrycode, T_Countries.name_german AS country
    FROM T_Companies
        INNER JOIN T_Countries
            ON T_Companies.fk_Country = T_Countries.pk_Country
    ORDER BY T_Companies.companyName='Privat' DESC, companyName
;

CREATE VIEW V_Employees AS
    SELECT
           T_Employees.pk_Employee as employeeid,
           T_Employees.firstname AS employeefirstname, T_Employees.lastname AS employeelastname,
           T_Employees.telephone AS employeemobilenumber
    FROM T_Employees
    ORDER BY employeelastname
;

CREATE VIEW V_Users AS
    SELECT
           T_Users.pk_User AS userid, T_Users.fk_Role AS roleid,
           T_Users.username, T_Users.password, T_Roles.role
    FROM T_Users
        INNER JOIN T_Roles
            ON T_Users.fk_Role = T_Roles.pk_Role
    ORDER BY userid
;

/* ########################################################################################################### */
-- Visitors
CREATE VIEW V_Visitors AS
    SELECT
           T_Visitors.pk_Visitor AS visitorid, T_Visitors.fk_Company AS companyid,
           T_Visitors.title, T_Visitors.firstname AS visitorfirstname, T_Visitors.lastname AS visitorlastname,
           T_Companies.Companyname AS company, T_Visitors.telephone AS mobilenumber
    FROM T_Visitors
        LEFT OUTER JOIN T_Companies
            ON T_Visitors.fk_Company = T_Companies.pk_Company
    ORDER BY company
;

CREATE VIEW V_VisitorsForSelect AS
SELECT
    T_Visitors.pk_Visitor AS visitorid, T_Visitors.fk_Company AS companyid,
    T_Visitors.firstname AS visitorfirstname, T_Visitors.lastname AS visitorlastname,
    T_Companies.Companyname AS company
FROM T_Visitors
         LEFT OUTER JOIN T_Companies
                         ON T_Visitors.fk_Company = T_Companies.pk_Company
ORDER BY visitorlastname
;

/* ########################################################################################################### */

CREATE VIEW V_Countries AS
    SELECT
           T_Countries.pk_Country AS countryid, T_Countries.name AS country_iso, T_Countries.name_german AS country, T_Countries.iso AS countrycode
    FROM T_Countries
    ORDER BY
             T_Countries.name='SWITZERLAND' DESC, T_Countries.name='GERMANY' DESC, T_Countries.name='AUSTRIA' DESC,
             T_Countries.name='LIECHTENSTEIN' DESC, T_Countries.name='FRANCE' DESC, T_Countries.name_german
;

CREATE VIEW V_WelcomeData AS
    SELECT
           T_WelcomeData.pk_WelcomeData AS welcomeid, T_WelcomeData.name AS name, T_WelcomeData.company AS company
    FROM T_WelcomeData
;
