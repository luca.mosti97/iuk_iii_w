DROP VIEW V_VisitsCombined;
DROP VIEW V_VisitsActiveForSinglePage;
DROP VIEW V_VisitsRecentlyDeparted;
DROP VIEW V_VisitsDeparted;
DROP VIEW V_Companies;
DROP VIEW V_Employees;
DROP VIEW V_Users;
DROP VIEW V_Visitors;
DROP VIEW V_VisitorsForSelect;
DROP VIEW V_Countries;
DROP VIEW V_WelcomeData;
DROP VIEW V_VisitsOverviewPage;
