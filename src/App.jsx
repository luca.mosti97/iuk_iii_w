import React from "react";
import {
    Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import Homescreen from "./main/Homescreen";
import VisitorsOverview from "./visitors/VisitorsOverview";
import Reports from "./reports/Reports";
import NewVisitor from "./visitor/newVisitor";
import NewCompany from "./company/NewCompany";
import ActiveVisit from "./visit/ActiveVisit";
import NewVisit from "./visit/newVisit";
import Emergency from "./emergency/emergency";
import WelcomeScreen from "./welcome/WelcomeScreen";
import NewEmployee from "./employee/NewEmployee";
import EmployeesOverview from "./employee/EmployeesOverview";
import WelcomeScreenInput from "./welcome/WelcomeScreenInput";
import Sidebar from "./main/Sidebar";
import NavigationBar from "./main/NavigationBar";
import ScrollToTop from "./ScrollToTop";

import "./App.css";

import history from "./historyDef";


function App() {
    return (
        <div className="App">
            <Router history={history}>
                <ScrollToTop />
                <header>
                    <NavigationBar />
                </header>
                <Container fluid>
                        <Switch>
                            <Route exact path={"/"}>
                                <Redirect to={"/home"} />
                            </Route>
                            <Route path={"/visitors-overview"}>
                                <Row>
                                    <Sidebar />
                                    <Col className={"col-xl-10 col-12"}>
                                        <VisitorsOverview />
                                    </Col>
                                </Row>
                            </Route>
                            <Route path={"/newVisitor"}>
                                <Row>
                                    <Sidebar />
                                    <Col className={"col-xl-10 col-12"}>
                                        <NewVisitor />
                                    </Col>
                                </Row>
                            </Route>
                            <Route path={"/newCompany"}>
                                <Row>
                                    <Sidebar />
                                    <Col className={"col-xl-10 col-12"}>
                                        <NewCompany />
                                    </Col>
                                </Row>
                            </Route>
                            <Route path={"/reports"}>
                                <Row>
                                    <Sidebar />
                                    <Col className={"col-xl-10 col-12"}>
                                        <Reports />
                                    </Col>
                                </Row>
                            </Route>
                            <Route path={"/newVisitor"}>
                                <Row>
                                    <Sidebar />
                                    <Col className={"col-xl-10 col-12"}>
                                        <NewVisitor />
                                    </Col>
                                </Row>
                            </Route>
                            <Route path={"/newCompany"}>
                                <Row>
                                    <Sidebar />
                                    <Col className={"col-xl-10 col-12"}>
                                        <NewCompany />
                                    </Col>
                                </Row>
                            </Route>
                            <Route path={"/newVisit"}>
                                <Row>
                                    <Sidebar />
                                    <Col className={"col-xl-10 col-12"}>
                                        <NewVisit />
                                    </Col>
                                </Row>
                            </Route>

                            <Route exact path={"/activeVisits/:id"} component={ActiveVisit} />

                            <Route path={"/emergency"}>
                                <Row>
                                    <Sidebar />
                                    <Col className={"col-xl-10 col-12"}>
                                        <Emergency />
                                    </Col>
                                </Row>
                            </Route>
                            <Route path={"/welcomeInput"}>
                                <Row>
                                    <Sidebar />
                                    <Col className={"col-xl-10 col-12"}>
                                        <WelcomeScreenInput />
                                    </Col>
                                </Row>
                            </Route>

                            <Route path={"/welcome"}>
                                <Row>
                                    <Col xs={12}>
                                        <WelcomeScreen />
                                    </Col>
                                </Row>
                            </Route>

                            <Route path={"/newEmployee"}>
                                <Row>
                                    <Sidebar />
                                    <Col className={"col-xl-10 col-12"}>
                                        <NewEmployee />
                                    </Col>
                                </Row>
                            </Route>
                            <Route path={"/Employee"}>
                                <Row>
                                    <Sidebar />
                                    <Col className={"col-xl-10 col-12"}>
                                        <EmployeesOverview />
                                    </Col>
                                </Row>
                            </Route>
                            <Route path={"/home"}>
                                <Row>
                                    <Sidebar />
                                    <Col className={"col-xl-10 col-12"}>
                                        <Homescreen />
                                    </Col>
                                </Row>
                            </Route>
                        </Switch>
                </Container>
            </Router>
        </div>
    );
}

export default App;
