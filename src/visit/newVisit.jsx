import React from "react";
import {Link} from "react-router-dom";
import Select from "react-select";

import Spinner from "react-bootstrap/Spinner";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form"
import Button from "react-bootstrap/Button";

import history from "../historyDef";

import PersonIcon from "../icons/feather/user-plus.svg";
import CancelIcon from "../icons/feather/x-circle.svg";


class NewVisit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            employees: [],
            visitors: [],
            employeeoption: "",
            visitoroption: "",
            badge: false,
            isLoadingVisitors: true,
            isLoadingEmployees: true,
            selectOptionsVisitor: [],
            selectOptionsEmployee: []
        };
        this.getOptionsVisitor = this.getOptionsVisitor.bind(this);
        this.getOptionsEmployee = this.getOptionsEmployee.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleInputChangeSelectVisitor = this.handleInputChangeSelectVisitor.bind(this);
        this.handleInputChangeSelectEmployee = this.handleInputChangeSelectEmployee.bind(this);
        this.addVisit = this.addVisit.bind(this);
    }

    /**
     * Initial Fetch Method
     */
    componentDidMount() {
        fetch("/api/allEmployees")
            .then((response => {
                return response.json();
            }))
            .then(data => {
               this.setState({
                   employees: data.map(d => ({
                       "employeeid": d.employeeid,
                       "employeefirstname": d.employeefirstname,
                       "employeelastname": d.employeelastname,
                   })),
                   employeeid: data[0].employeeid,
                   isLoadingEmployees: false
               })
            })
            .then(this.getOptionsEmployee)
        ;
        fetch("/api/allVisitorsSelect")
            .then((response => {
                return response.json();
            }))
            .then(data => {
                this.setState({
                    visitors: data.map(v => ({
                        "visitorid": v.visitorid,
                        "visitorfirstname": v.visitorfirstname,
                        "visitorlastname": v.visitorlastname,
                        "company": v.company
                    })),
                    //visitorid: data[0].visitorid,
                    isLoadingVisitors: false
                })
            })
            .then(this.getOptionsVisitor)
        ;
    }

    /**
     * For loading the options for Visitor react-select component
     * Gets called by {Link}
     * @returns {Promise<void>}
     */
    async getOptionsVisitor() {
        let list = this.state.visitors;
        let options = [];
        for (let i=0; i < list.length; i++) {
            options.push({
                "value": list[i].visitorid,
                "label": list[i].visitorlastname + ", " + list[i].visitorfirstname + " (" + list[i].company + ")"
            })
        }
        this.setState({
            selectOptionsVisitor: options
        })
    }

    /**
     * For loading the options for Employee react-select component
     * @returns {Promise<void>}
     */
    async getOptionsEmployee() {
        let list = this.state.employees;
        let options = [];
        for (let i=0; i < list.length; i++) {
            options.push({
                "value": list[i].employeeid,
                "label": list[i].employeelastname + ", " + list[i].employeefirstname
            })
        }
        this.setState({
            selectOptionsEmployee: options
        })
    }

    /**
     * Normal Handler for fields
     * @param event
     */
    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState( {
            [name]: value
        })
    }

    /**
     * Sets the option in Visitor select, value is in option.value, but state needs whole object
     * @param option    the new option from react-select component
     */
    handleInputChangeSelectVisitor(option) {
        this.setState( {
            visitoroption: option
        })
    }

    /**
     * Sets the option in Visitor select, value is in option.value, but state needs whole object
     * @param option    the new option from react-select component
     */
    handleInputChangeSelectEmployee(option) {
        this.setState( {
            employeeoption: option
        })
    }

    /**
     * POST method
     */
    addVisit(){
        let body = {
            visitorid: this.state.visitoroption.value,
            employeeid: this.state.employeeoption.value,
            badge: this.state.badge
        }

        fetch('/api/newVisit',{
            method: 'post',
            body: JSON.stringify(body),
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            }

        }).then(response => {
            if (response.status !== 201) {
                alert("Es gab einen Fehler beim Erfassen des Besuches. Versuch es nochmal");
            } else {
                history.push("/home");
                history.go(1);
            }
        });
    }

    render() {
        return(
            <main role={"main"} className={"ml-sm-auto px-md-4"}>
                <h2 className={"title"}>Neuen Besuch aufnehmen</h2>
                <Form className={"FormData"}>
                    <Form.Group>
                        <Form.Label htmlFor={"form-select-visitorid"}>Besucher <small> Nachname, Vorname (Firma) </small></Form.Label>
                        {
                            this.state.isLoadingVisitors ?
                                <Container>
                                    <div className={"spinnerContainer"}>
                                        <Spinner animation={"border"} role={"status"} aria-label={"Loading animation"}/>
                                    </div>
                                </Container>
                                :
                                <Select
                                    id={"form-select-visitorid"}
                                    name={"visitorid"}
                                    options={this.state.selectOptionsVisitor}
                                    onChange={this.handleInputChangeSelectVisitor}
                                    value={this.state.visitoroption}
                                    isSearchable={true}
                                    isClearable={true}
                                />
                        }
                    </Form.Group>
                        <Form.Group>
                            <Form.Label htmlFor={"form-select-employeeid"}>Zust&auml;ndiger Mitarbeiter <small> Nachname, Vorname</small></Form.Label>
                            {
                                this.state.isLoadingEmployees ?
                                    <Container>
                                        <div className={"spinnerContainer"}>
                                            <Spinner animation={"border"} role={"status"}
                                                     aria-label={"Loading animation"}/>
                                        </div>
                                    </Container>
                                    :
                                    <Select
                                        id={"form-select-employeeid"}
                                        name={"employeeid"}
                                        options={this.state.selectOptionsEmployee}
                                        onChange={this.handleInputChangeSelectEmployee}
                                        value={this.state.employeeoption}
                                        isSearchable={true}
                                        isClearable={true}
                                    />
                            }
                        </Form.Group>

                    <Form.Group>
                        <Form.Label htmlFor={"form-select-badge"}>Badge</Form.Label>
                        <Form.Control id={"form-select-badge"} as={"select"} name={"badge"} onChange={this.handleInputChange}>
                            <option value={false}>Nein</option>
                            <option value={true}>Ja</option>
                        </Form.Control>
                    </Form.Group>
                    <Row className={"form-button-bar-below"}>
                        <Col xs={12} sm={6}>
                            <Button className={"btn btn-nav newVisitorButton"} variant={"info"}
                            onClick={() => {this.addVisit()}}>
                                <img alt={"Person icon"} src={PersonIcon} className={"icons"} />&nbsp;Besuch&nbsp;erfassen
                            </Button>
                        </Col>
                        <Col xs={12} sm={6}>
                            <Button as={Link} to={"/home"} className={"button-float-right button-float-none-very-small-screen cancel-button"} variant={"secondary"}>
                                <img alt={"Cancel icon"} src={CancelIcon} className={"icons"} />&nbsp;Cancel
                            </Button>
                        </Col>
                    </Row>
                </Form>
            </main>
        )
    }
}

export default NewVisit;
