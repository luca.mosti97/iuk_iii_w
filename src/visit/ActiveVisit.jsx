import React from "react";
import {Link} from "react-router-dom";
import Select from "react-select";

import Form from "react-bootstrap/Form"
import Spinner from "react-bootstrap/Spinner";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";

import Sidebar from "../main/Sidebar.jsx";
import history from "../historyDef";

import "./ActiveVisit.css";

import CancelIcon from "../icons/feather/x-circle.svg";
import EditIcon from "../icons/feather/edit.svg";


class ActiveVisit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visitData: [],
            title: "",
            firstname: "",
            lastname: "",
            mobilenumber: "",
            arrival: "",
            employees: [],
            employeeid: "",
            employeeoption: "",
            selectOptionsEmployee: [],
            badge: "",
            isLoadingVisit: true,
            isLoadingEmployees: true
        };
        this.getOptionsEmployee = this.getOptionsEmployee.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleInputChangeSelectEmployee = this.handleInputChangeSelectEmployee.bind(this);
        this.changeVisitData = this.changeVisitData.bind(this);
        this.getActualEmployeeOption = this.getActualEmployeeOption.bind(this);
    }

    /**
     * Initial Fetch Method
     */
    componentDidMount() {
        fetch(`/api/activeVisits/${this.props.match.params.id}`)
            .then((response => {
                return response.json();
            })) 
            .then(data => {
                this.setState({
                    visitData: data[0],
                    title: data[0].title,
                    visitorfirstname: data[0].visitorfirstname,
                    visitorlastname: data[0].visitorlastname,
                    arrival: data[0].arrival,
                    badge: data[0].badge,
                    employeeid: data[0].employeeid,
                    isLoadingVisit: false
                });
                if (data[0].mobilenumber) {
                    this.setState({
                        mobilenumber: data[0].mobilenumber
                    })
                }
            })
            .then(this.getActualEmployeeOption)
        ;
        fetch("/api/allEmployees")
            .then((response => {
                return response.json();
            }))
            .then(data => {
                this.setState({
                    employees: data.map(d => ({
                        "employeeid": d.employeeid,
                        "employeefirstname": d.employeefirstname,
                        "employeelastname": d.employeelastname
                    })),
                    isLoadingEmployees: false
               })
            })
            .then(this.getOptionsEmployee)
            .then(this.getActualEmployeeOption)
        ;
    }

    /**
     * For loading the options for Employee react-select component
     * @returns {Promise<void>}
     */
    async getOptionsEmployee() {
        let list = this.state.employees;
        let options = [];
        for (let i=0; i < list.length; i++) {
            options.push({
                "value": list[i].employeeid,
                "label": list[i].employeelastname + ", " + list[i].employeefirstname
            })
        }
        this.setState({
            selectOptionsEmployee: options
        })
    }

    /**
     * Sets the option to be the currently associated employee, gets called in both fetch requests,
     * so that as soon as both are finished, the correct one gets set
     */
    getActualEmployeeOption() {
        if (!this.state.isLoadingVisit && !this.state.isLoadingEmployees) {
            let list = this.state.selectOptionsEmployee;
            for (let i = 0; i < list.length; i++) {
                if (list[i].value === this.state.employeeid) {
                    this.setState({
                        employeeoption: list[i]
                    })
                }
            }
        }
    }

    /**
     * Normal Handler for fields
     * @param event
     */
    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState( {
            [name]: value
        })
    }

    /**
     * Sets the option in Visitor select, value is in option.value, but state needs whole object
     * @param option    the new option from react-select component
     */
    handleInputChangeSelectEmployee(option) {
        this.setState( {
            employeeoption: option
        })
    }

    /**
     * PUT method
     */
    changeVisitData(){
        let body = {
            employeeid: this.state.employeeoption.value,
            badge: this.state.badge
        }

        fetch(`/api/changeVisit/${this.props.match.params.id}`,{
            method: 'put',
            body: JSON.stringify(body),
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            }
        })
            .then(response => {
                if (response.status !== 200) {
                    alert("Änderungen konnten nicht gespeichert werden. Bitte versuchen Sie es noch einmal.");
                } else {
                    history.push("/home")
                    history.go(1);
                }
            });
    }

    render() {
        return(
            <Row>
                <Sidebar />
                <Col className={"col-xl-10 col-12"}>
                    <main role={"main"} className={"ml-sm-auto px-lg-4"}>
                        <h2 className={"title"}>Besuchs&uuml;bersicht von {this.state.visitorfirstname} {this.state.visitorlastname}</h2>
                        {
                            this.state.isLoadingVisit ?
                                <Container>
                                    <div className={"spinnerContainer"}>
                                        <Spinner animation={"border"} role={"status"}
                                                 aria-label={"Loading animation"}/>
                                    </div>
                                </Container>
                                :
                                <Form className={"active-visit-form"}>
                                    <h3 className={"active-visit-subtitles"}>Besucher</h3>
                                    <Form.Group className={"FormData"}>
                                        <Form.Label htmlFor={"form-readonly-title"}>Anrede</Form.Label>
                                        <Form.Control id={"form-readonly-title"} className={"input-readonly"}
                                                      type={"text"} readOnly value={this.state.title}/>
                                        <Form.Label htmlFor={"form-readonly-firstname"}>Vorname</Form.Label>
                                        <Form.Control id={"form-readonly-firstname"}
                                                      className={"input-readonly"}
                                                      type={"text"} readOnly
                                                      value={this.state.visitorfirstname}/>
                                        <Form.Label htmlFor={"form-readonly-lastname"}>Nachname</Form.Label>
                                        <Form.Control id={"form-readonly-lastname"} className={"input-readonly"}
                                                      type={"text"} readOnly
                                                      value={this.state.visitorlastname}/>
                                        <Form.Label
                                            htmlFor={"form-readonly-mobilenumber"}>Telefonnummer</Form.Label>
                                        <Form.Control id={"form-readonly-mobilenumber"}
                                                      className={"input-readonly"}
                                                      type={"text"} readOnly value={this.state.mobilenumber}/>
                                    </Form.Group>
                                    <hr/>
                                    <h3 className={"active-visit-subtitles"}>Ankunft</h3>
                                    <Form.Group className={"FormData"}>
                                        <Form.Label htmlFor={"form-readonly-time"}>Zeit</Form.Label>
                                        <Form.Control id={"form-readonly-time"} className={"input-readonly"}
                                                      type={"text"} readOnly value={
                                            this.state.arrival.split(":")[0].split(" ")[1] + ":" +
                                            this.state.arrival.split(":")[1]
                                        }/>
                                        <Form.Label htmlFor={"form-readonly-date"}>Datum</Form.Label>
                                        <Form.Control id={"form-readonly-date"} className={"input-readonly"}
                                                      type={"text"} readOnly value={
                                            this.state.arrival.split(" ")[0].split("-")[2] + "." +
                                            this.state.arrival.split(" ")[0].split("-")[1] + "." +
                                            this.state.arrival.split(" ")[0].split("-")[0]
                                        }/>
                                    </Form.Group>
                                    <hr/>
                                    <h3 className={"active-visit-subtitles"}>Zus&auml;tzliches</h3>
                                    <Form.Group className={"FormData"}>
                                        <Form.Label htmlFor={"form-select-badge"}>Badge</Form.Label>
                                        <Form.Control as={"select"} id={"form-select-badge"}
                                                      name={"badge"}
                                                      value={this.state.badge}
                                                      onChange={this.handleInputChange}
                                                      custom>
                                            <option value={true}>Ja</option>
                                            <option value={false}>Nein</option>
                                        </Form.Control>
                                    </Form.Group>
                                    <Form.Group className={"FormData"}>
                                        <Form.Label htmlFor={"form-select-employeeid"}>Zust&auml;ndiger
                                            Mitarbeiter</Form.Label>
                                        {
                                            this.state.isLoadingEmployees ?
                                                <Container>
                                                    <div className={"spinnerContainer"}>
                                                        <Spinner animation={"border"} role={"status"}
                                                                 aria-label={"Loading animation"}/>
                                                    </div>
                                                </Container>
                                                :
                                                <Select
                                                    id={"form-select-employeeid"}
                                                    name={"employeeid"}
                                                    options={this.state.selectOptionsEmployee}
                                                    onChange={this.handleInputChangeSelectEmployee}
                                                    value={this.state.employeeoption}
                                                    menuPlacement={"top"}
                                                    isSearchable={true}
                                                    selectedValue={this.state.selectOptionsEmployee[this.state.employeeid]}
                                                />
                                        }
                                    </Form.Group>
                                    <Row className={"form-button-bar-below"}>
                                        <Col xs={12} sm={6}>
                                            <Button variant={"info"} onClick={() => {this.changeVisitData()}}>
                                                <img alt={"Person icon"} src={EditIcon} className={"icons"}/>&nbsp;Daten&nbsp;&auml;ndern
                                            </Button>
                                        </Col>
                                        <Col xs={12} sm={6}>
                                            <Link to={"/home"}>
                                                <Button
                                                    className={"button-float-right button-float-none-very-small-screen cancel-button"} variant={"secondary"}>
                                                    <img alt={"Cancel icon"} src={CancelIcon} className={"icons"}/>&nbsp;Cancel
                                                </Button>
                                            </Link>
                                        </Col>
                                    </Row>
                                </Form>
                        }
                    </main>

                </Col>
            </Row>
        )
    }
}

export default ActiveVisit;