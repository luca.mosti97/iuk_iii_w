import React /*, {useState}*/ from "react";
//import DatePicker from "react-datepicker";

import Container from "react-bootstrap/Container";
import Spinner from "react-bootstrap/Spinner";
import Form from "react-bootstrap/Form";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";

import ReportsVisitTable from "./ReportsVisitTable";

import ReportIcon from "../icons/feather/clipboard.svg";
import Select from "react-select";


class Reports extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visitors: [],
            companies: [],
            visits: [],
            arrival: "",
            departure: "",
            firstLoad: true,
            isSearching: false,
            visitoroption: "",
            companyoption: "",
            selectOptionsVisitors: [],
            selectOptionsCompanies: [],
            isLoadingVisitors: true,
            isLoadingCompanies: true,
            errorText: ""
        };
        this.getOptionsVisitor = this.getOptionsVisitor.bind(this);
        this.getOptionsCompany = this.getOptionsCompany.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleInputChangeSelectVisitor = this.handleInputChangeSelectVisitor.bind(this);
        this.handleInputChangeSelectCompany = this.handleInputChangeSelectCompany.bind(this);
        this.generateReport = this.generateReport.bind(this);
    }

    /**
     * Initial Fetch Method
     */
    componentDidMount(){
        fetch("/api/allVisitorsSelect")
            .then((response => {
                return response.json();
            }))
            .then(data => {
                this.setState({
                    visitors: data.map(v => ({
                        "visitorid": v.visitorid,
                        "visitorfirstname": v.visitorfirstname,
                        "visitorlastname": v.visitorlastname,
                        "company": v.company
                    })),
                    isLoadingVisitors: false
                })
            })
            .then(this.getOptionsVisitor)
        ;
        fetch("/api/allCompanies")
            .then((response => {
                return response.json();
            }))
            .then(data => {
                this.setState({
                    companies: data.map(c => ({
                        "companyid": c.companyid,
                        "companyname": c.company
                    })),
                    isLoadingCompanies: false
                })
            })
            .then(this.getOptionsCompany)
        ;
    }

    /**
     * For loading the options for Visitor react-select component
     * Gets called by {Link}
     * @returns {Promise<void>}
     */
    async getOptionsVisitor() {
        let list = this.state.visitors;
        let options = [];
        for (let i=0; i < list.length; i++) {
            options.push({
                "value": list[i].visitorid,
                "label": list[i].visitorlastname + ", " + list[i].visitorfirstname + " (" + list[i].company + ")"
            })
        }
        this.setState({
            selectOptionsVisitors: options
        })
    }

    /**
     * For loading the options for Company react-select component
     *
     * @returns {Promise<void>}
     */
    async getOptionsCompany() {
        let list = this.state.companies;
        let options = [];
        for (let i=0; i < list.length; i++) {
            options.push({
                "value": list[i].companyid,
                "label": list[i].companyname
            })
        }
        this.setState({
            selectOptionsCompanies: options
        })
    }

    /**
     * Normal Handler for fields
     * @param event
     */
    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        if (name === "arrival" || name === "departure") {
            //let dateInFutureTest = (new Date().toISOString()) < value;
            //console.log(dateInFutureTest)
        }

        this.setState( {
            [name]: value
        })
    }

    /**
     * Sets the option in Visitor select, value is in option.value, but state needs whole object
     * @param option    the new option from react-select component
     */
    handleInputChangeSelectVisitor(option) {
        this.setState( {
            visitoroption: option
        })
    }

    /**
     * Sets the option in Company select, value is in option.value, but state needs whole object
     * @param option    the new option from react-select component
     */
    handleInputChangeSelectCompany(option) {
        this.setState( {
            companyoption: option
        })
    }

    /**
     * GET Method
     */
    generateReport() {
        let visitorid = "";
        let companyid = "";
        if (this.state.visitoroption.value) {
            visitorid = this.state.vistoroption.value;
        }
        if (this.state.companyoption.value) {
            companyid = this.state.companyoption.value;
        }
        this.setState({isSearching: true});
        const url = `/api/reports?visitorid=${visitorid}&companyid=${companyid}&arrivaldate=${this.state.arrival}&departuredate=${this.state.departure}`
        fetch(url)
            .then((response => {
                this.setState({firstLoad: false});
                if (response.status === 403) {
                    this.setState({errorText: "Bitte schr\u00E4nken Sie die Suche ein"});
                    return response.json();
                }
                if (response.status === 404) {
                    this.setState({errorText: "Mit den aktuellen Suchbegriffen wurden keine Besuche gefunden"});
                    return response.json();
                }
                if (response.status === 422) {
                    this.setState({errorText: "Abreisedatum ist vor dem Ankunftsdatum"});
                    return response.json();
                }
                if (response.status !== 200) {
                    this.setState({errorText: "Unerwarteter Fehler aufgetreten"});
                    return response.json();
                }
                this.setState({errorText: ""});
                return response.json();
            }))
            .then(data => {
                this.setState({
                    visits: data,
                    isSearching: false
                })
            });
    }

    render() {
        return(
            <main role={"main"} className={"ml-sm-auto px-md-4"}>
                <h2>Reports erstellen</h2>
                <Form.Group>
                    <Form.Label htmlFor={"form-select-visitorid"}>Besucher <small>| Nachname, Vorname (Firma) |</small></Form.Label>
                    {
                        this.state.isLoadingVisitors ?
                            <Container>
                                <div className={"spinnerContainer"}>
                                    <Spinner animation={"border"} role={"status"} aria-label={"Loading animation"}/>
                                </div>
                            </Container>
                            :
                            <Select
                                id={"form-select-visitorid"}
                                name={"visitorid"}
                                options={this.state.selectOptionsVisitors}
                                onChange={this.handleInputChangeSelectVisitor}
                                value={this.state.visitoroption}
                                isSearchable={true}
                                isClearable={true}
                            />
                    }
                </Form.Group>
                <Form.Group>
                    <Form.Label htmlFor={"form-select-companyid"}>Firma</Form.Label>
                    {
                        this.state.isLoadingCompany ?
                            <Container>
                                <div className={"spinnerContainer"}>
                                    <Spinner animation={"border"} role={"status"} aria-label={"Loading animation"}/>
                                </div>
                            </Container>
                            :
                            <Select
                                id={"form-select-companyid"}
                                name={"companyid"}
                                options={this.state.selectOptionsCompanies}
                                onChange={this.handleInputChangeSelectCompany}
                                value={this.state.companyoption}
                                isSearchable={true}
                                isClearable={true}
                            />
                    }
                </Form.Group>
                <Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor={"form-arrival"}>Ankunft</Form.Label>
                        <Form.Control id={"form-arrival"} name={"arrival"} type={"date"} onChange={this.handleInputChange} />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor={"form-departure"}>Abreise</Form.Label>
                        <Form.Control id={"form-departure"} name={"departure"} type={"date"} onChange={this.handleInputChange} />
                    </Form.Group>
                </Form.Group>
                <Button variant={"info"} onClick={() => {this.generateReport()}}>
                    <img alt={"Report icon"} src={ReportIcon} className={"icons"} />&nbsp;Report&nbsp;generieren
                </Button>
                {
                    this.state.isSearching ?
                        <Container>
                            <div className={"spinnerContainer"}>
                                <Spinner animation={"border"} role={"status"} aria-label={"Loading animation"}/>
                            </div>
                        </Container>
                        :
                        null
                }
                <hr />
                <Table striped hover responsive className={"tAllVisitors"}>
                    <thead className={"thead-dark"}>
                        <tr>
                            <th>Vorname</th>
                            <th>Nachname</th>
                            <th>Firma</th>
                            <th>Mitarbeiter</th>
                            <th>Badge</th>
                            <th>Ankunft</th>
                            <th>Abreise&nbsp;&#9650;</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.visits.length > 0 ?
                            this.state.visits.map(function (tVisitsReport) {
                                return (<ReportsVisitTable key={tVisitsReport.visitid} tVisitsReport={tVisitsReport} />)
                            })
                            :
                                this.state.firstLoad ?
                                    <tr>
                                        <td>Noch keine Suche gestartet</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    :
                                    <tr>
                                        <td>{this.state.errorText}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                        }
                    </tbody>
                </Table>
            </main>
        )
    }
}


export default Reports;