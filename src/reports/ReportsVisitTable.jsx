import React from "react";


class ReportsVisitTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    

    render() {
        return(
            <tr>
                <td>{this.props.tVisitsReport.visitorfirstname}</td>
                <td>{this.props.tVisitsReport.visitorlastname}</td>
                <td>{this.props.tVisitsReport.company}</td>
                <td>{this.props.tVisitsReport.employeefirstname} {this.props.tVisitsReport.employeelastname}</td>
                <td>{
                    this.props.tVisitsReport.badge ?
                        "Ja"
                        :
                        "Nein"
                }</td>
                <td>{this.props.tVisitsReport.arrival.split(" ")[0]} {this.props.tVisitsReport.arrival.split(" ")[1].split(":")[0]}:{this.props.tVisitsReport.arrival.split(" ")[1].split(":")[1]} Uhr</td>
                <td>{this.props.tVisitsReport.departure.split(" ")[0]} {this.props.tVisitsReport.departure.split(" ")[1].split(":")[0]}:{this.props.tVisitsReport.departure.split(" ")[1].split(":")[1]} Uhr</td>
            </tr>   
        )
    }
}

export default ReportsVisitTable;