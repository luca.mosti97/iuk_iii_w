import React from "react";
import {Link} from "react-router-dom";

import Container from "react-bootstrap/Container";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import Spinner from "react-bootstrap/Spinner";

import CancelIcon from "../icons/feather/x-circle.svg";

import EmergencyVisitorsTable from "../visitors/EmergencyVisitorsTable";

import "../main/main.css";

class Emergency extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visitors: [],
            lastDeparted: [],
            isLoading: true
        };
    }

    componentDidMount() {
        fetch("/api/activeVisits")
            .then((response => {
                return response.json();
            }))
            .then(data => {
                this.setState({
                    visitors: data,
                    isLoading: false
                })
            });
    }

    render() {
        return (
            <main role={"main"} className={"ml-sm-auto px-md-4"}>
                <h2>Notfallmodus</h2>
                {
                    this.state.isLoading ?
                        <Container>
                            <div className={"spinnerContainer"}>
                                <Spinner animation={"border"} role={"status"} aria-label={"Loading animation"}/>
                            </div>
                        </Container>
                        :
                        <Table striped hover responsive>
                            <thead className={"thead-dark"}>
                            <tr>
                                <th>Datum</th>
                                <th>Ankunft&nbsp;&#9650;</th>
                                <th>Firma</th>
                                <th>Name</th>
                                <th>Vorname</th>
                                <th>Zuständiger Mitarbeiter</th>
                                <th>Gefunden</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                this.state.visitors.length > 0 ?
                                    this.state.visitors.map(function (tEmergency) {
                                        return (
                                            <EmergencyVisitorsTable key={tEmergency.visitid} tEmergency={tEmergency}/>)
                                    })
                                    :
                                    <tr>
                                        <td>Keine Besucher vorhanden</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                            }
                            </tbody>
                        </Table>
                }
                <Button as={Link} to={"/home"} className={"button-float-right button-float-none-very-small-screen cancel-button"} variant={"secondary"}>
                    <img alt={"Cancel icon"} src={CancelIcon} className={"icons"}/>&nbsp;Cancel
                </Button>
            </main>
        )
    }
}

export default Emergency;
