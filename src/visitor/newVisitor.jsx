import React from "react";
import {Link} from "react-router-dom";
import Select from "react-select";

import Container from "react-bootstrap/Container";
import Spinner from "react-bootstrap/Spinner";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

import history from "../historyDef";

import PersonIcon from "../icons/feather/user-plus.svg";
import CancelIcon from "../icons/feather/x-circle.svg";


class NewVisitor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            companies: [],
            companyid: 1,
            companyname: "",
            title: "Frau",
            firstname: "",
            lastname: "",
            mobilenumber: "",
            companyoption: "",
            selectOptionsCompanies: [],
            isLoadingCompany: true
        };
        this.getOptionsCompany = this.getOptionsCompany.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleInputChangeSelectCompany = this.handleInputChangeSelectCompany.bind(this);
        this.getDefaultCompanyOption = this.getDefaultCompanyOption.bind(this);
        this.addVisitor = this.addVisitor.bind(this);
    }

    /**
     * Initial Fetch Method
     */
    componentDidMount() {
        fetch("/api/allCompanies")
            .then((response => {
                return response.json();
            }))
            .then(data => {
                this.setState({
                    companies: data.map(c => ({
                        "companyid": c.companyid,
                        "companyname": c.company
                    })),
                    isLoadingCompany: false
                })
            })
            .then(this.getOptionsCompany)
            .then(this.getDefaultCompanyOption)
        ;
    }

    /**
     * For loading the options for Company react-select component
     *
     * @returns {Promise<void>}
     */
    async getOptionsCompany() {
        let list = this.state.companies;
        let options = [];
        for (let i=0; i < list.length; i++) {
            options.push({
                "value": list[i].companyid,
                "label": list[i].companyname
            })
        }
        this.setState({
            selectOptionsCompanies: options
        })
    }

    /**
     * Normal Handler for fields
     * @param event
     */
    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState( {
            [name]: value
        })
    }

    /**
     * Sets the option in Company select, value is in option.value, but state needs whole object
     * @param option    the new option from react-select component
     */
    handleInputChangeSelectCompany(option) {
        this.setState( {
            companyoption: option
        })
    }

    /**
     * Sets the option to be the first Company
     */
    async getDefaultCompanyOption() {
        let list = this.state.selectOptionsCompanies;
        for (let i = 0; i < list.length; i++) {
            if (list[i].value === this.state.companyid) {
                this.setState({
                    companyoption: list[i]
                })
            }
        }
    }

    /**
     * POST method
     */
    addVisitor(){
        let body = {
            title: this.state.title,
            firstname: this.state.firstname,
            lastname: this.state.lastname,
            companyid: this.state.companyoption.value,
            mobilenumber: this.state.mobilenumber
        }

        fetch('/api/newVisitor',{
            method: 'post',
            body: JSON.stringify(body),
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            }

        }).then(response => {
            if (response.status !== 201) {
                alert("Bitte fülle alle Felder aus!");
            } else {
                //alert("Besucher wurde erfasst!");
                history.push("/home")
                history.go(1);
            }
        });
    }

    render() {
        return(
            <main role={"main"} className={"ml-sm-auto px-md-4"}>
                <h2>Neuen Besucher hinzuf&uuml;gen</h2>
                <Form>
                    <Form.Group>
                        <Form.Label htmlFor={"form-title"}>Anrede</Form.Label>
                        <Form.Control as={"select"} id={"form-title"}
                            className={"mr-sm-2"}
                            name={"title"}
                            onChange={this.handleInputChange}
                            custom>
                                <option value="Frau">Frau</option>
                                <option value="Herr">Herr</option>
                        </Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor={"form-firstname"}>Vorname</Form.Label>
                        <Form.Control id={"form-firstname"} name={"firstname"} type={"text"} placeholder={"Vorname"} onChange={this.handleInputChange} />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor={"form-lastname"}>Nachname</Form.Label>
                        <Form.Control id={"form-lastname"} name={"lastname"} type={"text"} placeholder={"Nachname"} onChange={this.handleInputChange}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor={"form-mobilenumber"}>Telefonnummer <small>(Optional)</small></Form.Label>
                        <Form.Control id={"form-mobilenumber"} name={"mobilenumber"} type={"text"} placeholder={"Telefonnummer"} onChange={this.handleInputChange}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor={"form-select-companyid"}>Firma</Form.Label>
                        {
                            this.state.isLoadingCompany ?
                                <Container>
                                    <div className={"spinnerContainer"}>
                                        <Spinner animation={"border"} role={"status"} aria-label={"Loading animation"}/>
                                    </div>
                                </Container>
                                :
                                <Select
                                    id={"form-select-companyid"}
                                    name={"companyid"}
                                    options={this.state.selectOptionsCompanies}
                                    onChange={this.handleInputChangeSelectCompany}
                                    value={this.state.companyoption}
                                    menuPlacement={"top"}
                                    isSearchable={true}
                                    selectedValue={this.state.selectOptionsCompanies[this.state.companyid]}
                                />
                        }
                    </Form.Group>
                    <Row className={"form-button-bar-below"}>
                        <Col xs={12} sm={6}>
                            <Button variant={"info"} onClick={() => {this.addVisitor()}}>
                                <img alt={"Person icon"} src={PersonIcon} className={"icons"} />&nbsp;Neuer&nbsp;Besucher
                            </Button>
                        </Col>
                        <Col xs={12} sm={6}>
                            <Button as={Link} to={"/home"} className={"button-float-right button-float-none-very-small-screen cancel-button"} variant={"secondary"}>
                                <img alt={"Cancel icon"} src={CancelIcon} className={"icons"} />&nbsp;Cancel
                            </Button>
                        </Col>
                    </Row>
                </Form>
            </main>
        )
    }
}

export default NewVisitor;
