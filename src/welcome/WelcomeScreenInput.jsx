import React from "react";
import {Link} from "react-router-dom";

import Form from "react-bootstrap/Form"
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import history from "../historyDef";

import SaveIcon from "../icons/feather/save.svg";
import CancelIcon from "../icons/feather/x-circle.svg";
import MonitorIcon from "../icons/feather/monitor.svg";


class WelcomeScreenInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            company: ""
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.updateWelcomeData = this.updateWelcomeData.bind(this);
        this.setDefaultWelcomeData = this.setDefaultWelcomeData.bind(this);
    }


    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState( {
            [name]: value
        })
    }

    updateWelcomeData() {
        let body = {
            name: this.state.name,
            company: this.state.company
        }

        fetch('/api/welcomeData',{
            method: 'put',
            body: JSON.stringify(body),
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            }
        })
            .then(response => {
                if (response.status !== 200) {
                    alert("Beim Aktualisieren der Willkommensanzeige gab es einen Fehler. Bitte versuchen Sie es noch einmal");
                } else {
                    history.push("/welcome")
                    history.go(1);
                }
            });
    }

    setDefaultWelcomeData() {
        fetch('/api/defaultWelcomeData',{
            method: 'put',
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            }
        })
            .then(response => {
                if (response.status !== 200) {
                    alert("Beim Aktualisieren der Daten gab es einen Fehler. Bitte versuchen Sie es noch einmal");
                } else {
                    history.push("/welcome")
                    history.go(1);
                }
            });
    }

    render() {
        return(
            <main role={"main"} className={"ml-sm-auto px-md-4"}>
                <Row>
                    <Col xs={12} md={8}>
                        <h2>Willkommensanzeige erstellen</h2>
                    </Col>
                    <Col xs={12} md={4}>
                        <Button as={Link} to={"/Welcome"} className={"btn button-welcomescreen-link"} variant={"primary"}>
                            <img alt={"Person icon"} src={MonitorIcon} id={"logo-nav-button"} />&nbsp;Welcome&nbsp;Screen
                        </Button>
                    </Col>
                </Row>
                <Form>
                    <Form.Group>
                        <Form.Label htmlFor={"form-name"}>Vollst&auml;ndiger Name</Form.Label>
                        <Form.Control id={"form-name"} name="name" type="text" placeholder="Name" onChange={this.handleInputChange} />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor={"form-company"}>Firma <small>(Optional)</small></Form.Label>
                        <Form.Control id={"form-company"} name="company" type="text" placeholder="Firma" onChange={this.handleInputChange}/>
                    </Form.Group>
                    <Row className={"form-button-bar-below"}>
                        <Col xs={12} sm={6} className={"order-1 order-sm-1"}>
                            <Button variant={"info"} className={"welcome-screen-action-button"} onClick={() => {this.updateWelcomeData()}}>
                                <img alt={"Save icon"} src={SaveIcon} className={"icons"} />&nbsp;Daten&nbsp;speichern
                            </Button>
                        </Col>
                        <Col xs={12} sm={6} className={"order-3 order-sm-2"}>
                            <Button as={Link} to={"/home"} className={"button-float-right button-float-none-very-small-screen cancel-button"} variant={"secondary"}>
                                <img alt={"Cancel icon"} src={CancelIcon} className={"icons"} />&nbsp;Cancel
                            </Button>
                        </Col>
                        <Col xs={12} className={"order-2 order-sm-3"}>
                            <Button variant={"success"} id={"second-row-button"} className={"welcome-screen-action-button"} onClick={() => {this.setDefaultWelcomeData()}}>
                                <img alt={"Save icon"} src={SaveIcon} className={"icons"} />&nbsp;Standarddaten
                            </Button>
                        </Col>
                    </Row>
                </Form>
            </main>
        )
    }
}

export default WelcomeScreenInput;
