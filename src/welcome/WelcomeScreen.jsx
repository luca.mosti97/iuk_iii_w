import React from "react";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import CompanyImage from "../images/not_showing_company_image.jpg";

import "./WelcomeScreen.css";


class WelcomeScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            company: "",
            isLoading: true
        };
    }

    componentDidMount(){
        fetch("/api/welcomeData")
            .then((response => {
                return response.json();
            }))
            .then(data => {
                this.setState({
                    name: data[0].name,
                    company: data[0].company,
                    isLoading: false
            })
        });
    }

    render() {
        return(
            <section>
                <Container fluid>
                    <Row className={"welcome-screen-row"}>
                        <Col xs={12} className={"welcome-screen-col"}>
                            <h1 id={"welcome-screen-name"}>Herzlich Willkommen <span className={"welcome-screen-dynamic-input"}>{this.state.name}</span></h1>
                        </Col>
                        {
                            this.state.company.length > 0 ?
                                <Col xs={12} className={"welcome-screen-col"}>
                                    <h2 id={"welcome-screen-company"}>der Firma <span className={"welcome-screen-dynamic-input"}>{this.state.company}</span></h2>
                                </Col>
                                :
                                null
                        }
                    </Row>
                    <Row>
                        <Col xs={12} className={"welcome-screen-col"}>
                            <h2>bei</h2>
                        </Col>
                        <Col xs={12} id={"companylogo"}>
                            <img src={CompanyImage} alt={"WYON AG Logo"} />
                        </Col>
                    </Row>
                </Container>
            </section>

        )
    }
}


export default WelcomeScreen;
