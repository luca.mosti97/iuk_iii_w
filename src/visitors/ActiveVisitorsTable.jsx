import React from "react";
import { Link } from "react-router-dom";

import Button from "react-bootstrap/Button";
import EditIcon from "../icons/feather/edit.svg";
import LeaveIcon from "../icons/feather/log-out.svg"

import history from "../historyDef";


class ActiveVisitorsTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visitors: this.props.tActiveVisits
        };
        this.VisitorLeaving = this.VisitorLeaving.bind(this);
    }

    VisitorLeaving() {
        fetch(`/api/visit/${this.props.tActiveVisits.visitid}`,{
            method: 'put'
        })
            .then(response => {
                if (response.status === 201) {
                    //history.push("/home");
                    history.go(0);
                } else {
                    alert("Beim Vorgang ist ein Fehler aufgetreten.");
                }
            });
    }

    render() {
        return(
            <tr>
                <td>{this.props.tActiveVisits.arrival.split(" ")[0]}</td>
                <td>{this.props.tActiveVisits.arrival.split(" ")[1].split(":")[0]}:{this.props.tActiveVisits.arrival.split(" ")[1].split(":")[1]} Uhr</td>
                <td>{this.props.tActiveVisits.company}</td>
                <td>{this.props.tActiveVisits.visitorlastname}</td>
                <td>{this.props.tActiveVisits.visitorfirstname}</td>
                <td>{this.props.tActiveVisits.employeelastname} {this.props.tActiveVisits.employeefirstname}</td>
                <td>
                    <Button as={Link} to={`/activeVisits/${this.props.tActiveVisits.visitid}`} variant={"info"}>
                        <img alt={"Person icon"} src={EditIcon} />
                    </Button>
                </td>
                <td>
                    <Button variant={"dark"} onClick={() => {this.VisitorLeaving()}}>
                        <img alt={"Leave Icon"} src={LeaveIcon} />
                    </Button>
                </td>
            </tr>   
        )
    }
}

export default ActiveVisitorsTable;
