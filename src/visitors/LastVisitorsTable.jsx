import React from "react";
import { Link } from "react-router-dom";

import Button from "react-bootstrap/Button";

import history from "../historyDef";

import ReactivateIcon from "../icons/feather/log-in.svg";


class LastVisitorsTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.ReactivateVisitor = this.ReactivateVisitor.bind(this);
    }

    ReactivateVisitor() {
        let body = {
            visitid: this.props.tLastDeparted.visitid
        }
        fetch('/api/reactivateVisit ',{
            method: 'post',
            body: JSON.stringify(body),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => {
            if (response.status === 201) {
                //alert("Besuch erfolgreich reaktiviert");
                //history.push("/home");
                history.go(0);
            } else {
                alert("Beim Reaktivieren des Besuchers ist ein Fehler aufgetreten.")
            }
        });
    }

    render() {
        return(
            <tr>
                <td>{this.props.tLastDeparted.departure.split(" ")[0]}</td>
                <td>{this.props.tLastDeparted.departure.split(" ")[1].split(":")[0]}:{this.props.tLastDeparted.departure.split(" ")[1].split(":")[1]} Uhr</td>
                <td>{this.props.tLastDeparted.company}</td>
                <td>{this.props.tLastDeparted.visitorlastname}</td>
                <td>{this.props.tLastDeparted.visitorfirstname}</td>
                <td>{this.props.tLastDeparted.employeelastname} {this.props.tLastDeparted.employeefirstname}</td>
                <td>
                    <Button as={Link} to={"/visitor/"} className={"btn"} variant={"success"} onClick={() => {this.ReactivateVisitor()}}>
                        <img alt={"Reactivate icon"} src={ReactivateIcon} />
                    </Button>
                </td>
            </tr>
        )
    }
}

export default LastVisitorsTable;