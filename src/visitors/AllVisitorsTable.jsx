import React from "react";

class AllVisitorsTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return(
            <tr>
                <td>{this.props.tAllVisitors.company}</td>
                <td>{this.props.tAllVisitors.title}</td>
                <td>{this.props.tAllVisitors.visitorfirstname}</td>
                <td>{this.props.tAllVisitors.visitorlastname}</td>
            </tr>
        )
    }
}

export default AllVisitorsTable;