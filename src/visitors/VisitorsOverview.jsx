import React from "react";

import Container from "react-bootstrap/Container";
import Spinner from "react-bootstrap/Spinner";
import Table from "react-bootstrap/Table";
import Form from "react-bootstrap/Form"
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";

import AllVisitorsTable from "./AllVisitorsTable";

import SearchIcon from "../icons/feather/search.svg";

import "./visitorsOverview.css";
import "../main/main.css";
import "../main/main.css";
//import "react-table-filter/lib/styles.css";


class VisitorsOverview extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visitors: [],
            searchFirstname: "",
            searchLastname: "",
            searchingProcess: false,
            errorText: "",
            isLoading: true
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.searchForVisitor = this.searchForVisitor.bind(this);
    }

    componentDidMount(){
        fetch("/api/allVisitors")
            .then((response => {
                return response.json();
            }))
            .then(data => {
                this.setState({
                    visitors: data,
                    isLoading: false
                })
            });
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState( {
            [name]: value
        })

    }

    searchForVisitor() {
        this.setState({searchingProcess: true})
        fetch(`/api/searchForVisitor?firstname=${this.state.searchFirstname}&lastname=${this.state.searchLastname}`)
            .then((response => {
                if (response.status === 404) {
                    this.setState({errorText: "Besucher nicht gefunden"});
                    return response.json();
                }
                if (response.status !== 200) {
                    this.setState({errorText: "Ein Fehler ist aufgetreten"});
                    return response.json();
                }
                this.setState({errorText: ""});
                return response.json();
            }))
            .then(data => {
                this.setState({
                    visitors: data,
                    searchingProcess: false
                });
            });
        this.render();
    }

    render() {
        return(
            <main role={"main"} className={"ml-sm-auto px-lg-4"}>
                <Container fluid>
                    <Form.Row>
                        <Col xs={12} md={5}>
                            <Form.Group>
                                <Form.Control className={"form-control-input"} name={"searchFirstname"}  type={"text"} placeholder={"Vorname"} onChange={this.handleInputChange} />
                            </Form.Group>
                        </Col>
                        <Col xs={12} md={5}>
                            <Form.Group>
                                <Form.Control className={"form-control-input"} name={"searchLastname"} type={"text"} placeholder={"Nachname"} onChange={this.handleInputChange}/>
                            </Form.Group>
                        </Col>
                        <Col xs={"auto"} lg={2} className={"ml-auto"}>
                            <Button className={"search-button"} variant={"info"} onClick={() => {this.searchForVisitor()}}>
                                <img alt={"Search icon"} src={SearchIcon} id={"icons"} />&nbsp;Suche
                            </Button>  
                        </Col>
                    </Form.Row>
                </Container>
                {
                    this.state.searchingProcess ?
                        <Container>
                            <div className={"spinnerContainer"}>
                                <Spinner animation={"border"} role={"status"} aria-label={"Ladeanimation"} />
                            </div>
                        </Container>
                        :null
                }
                <hr />
                {
                    this.state.isLoading ?
                        <Container>
                            <div className={"spinnerContainer"}>
                                <Spinner animation={"border"} role={"status"} aria-label={"Loading animation"}/>
                            </div>
                        </Container>
                        :

                        <Table striped hover responsive className={"tAllVisitors"}>
                            <thead className={"thead-dark"}>
                            <tr>
                                <th>Firma&nbsp;&#9660;&nbsp;A-Z</th>
                                <th>Titel</th>
                                <th>Vorname</th>
                                <th>Nachname</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                this.state.visitors.length > 0 ?
                                    this.state.visitors.map(function (tAllVisitors) {
                                        return (<AllVisitorsTable key={tAllVisitors.visitorid}
                                                                  tAllVisitors={tAllVisitors}/>)
                                    })
                                    :
                                    <tr>
                                        <td>{this.state.errorText}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                            }
                            </tbody>
                        </Table>
                }
            </main>
        )
    }
}

export default VisitorsOverview;
