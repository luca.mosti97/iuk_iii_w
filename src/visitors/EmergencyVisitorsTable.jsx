import React from "react";

import Form from "react-bootstrap/Form";


class EmergencyVisitorsTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visitors: this.props.tEmergency
        };
    }

    render() {
        return(
            <tr>
                <td>{this.props.tEmergency.arrival.split(" ")[0]}</td>
                <td>{this.props.tEmergency.arrival.split(" ")[1].split(":")[0]}:{this.props.tEmergency.arrival.split(" ")[1].split(":")[1]} Uhr</td>
                <td>{this.props.tEmergency.company}</td>
                <td>{this.props.tEmergency.visitorlastname}</td>
                <td>{this.props.tEmergency.visitorfirstname}</td>
                <td>{this.props.tEmergency.employeelastname} {this.props.tEmergency.employeefirstname}</td>
                <td>
                    <Form.Check className={"emergeny-checkbox-found"} aria-label="option 1" />
                </td>
            </tr>   
        )
    }
}

export default EmergencyVisitorsTable;