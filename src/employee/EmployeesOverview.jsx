import React from "react";
import {Link} from "react-router-dom";

import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Spinner from "react-bootstrap/Spinner";

import AllEmployeesTable from "./AllEmployeesTable";

import PersonIcon from "../icons/feather/user-plus.svg";

import "../visitors/visitorsOverview.css";
import "../main/main.css";

class EmployeesOverview extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            employees: [],
            isLoading: true
        };
    }

    componentDidMount(){
        fetch("/api/allEmployees")
            .then((response => {
                return response.json();
            }))
            .then(data => {
                this.setState({
                    employees: data,
                    isLoading: false
                })
            });
    }

    render() {
        return(
            <main role={"main"} className={"ml-sm-auto px-md-4"}>
                <Container fluid>
                    <Row>
                        <Col>
                            <Button as={Link} to={"/newEmployee"} className={"button-float-right"} variant={"info"}>
                                <img alt={"Person icon"} src={PersonIcon} className={"icons"} />&nbsp;Neuer&nbsp;Mitarbeiter
                            </Button>
                        </Col>
                    </Row>
                </Container>
                <hr />
                {
                    this.state.isLoading ?
                        <Container>
                            <div className={"spinnerContainer"}>
                                <Spinner animation={"border"} role={"status"} aria-label={"Loading animation"}/>
                            </div>
                        </Container>
                        :
                        <Table striped hover responsive className={"tAllVisitors"}>
                            <thead className={"thead-dark"}>
                            <tr>
                                <th>Nachname&nbsp;&#9660;&nbsp;A-Z</th>
                                <th>Vorname</th>
                                <th>Telefonnummer</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                this.state.employees.length > 0 ?
                                    this.state.employees.map(function (tAllEmployees) {
                                        return (<AllEmployeesTable key={tAllEmployees.employeeid}
                                                                   tAllEmployees={tAllEmployees}/>)
                                    })
                                    :
                                    <tr>
                                        <th>Keine Mitarbeiter vorhanden</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                            }
                            </tbody>
                        </Table>
                }
            </main>
        )
    }
}


export default EmployeesOverview;
