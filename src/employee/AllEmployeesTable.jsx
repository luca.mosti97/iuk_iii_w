import React from "react";

class AllEmployeesTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return(
            <tr>
                <td>{this.props.tAllEmployees.employeelastname}</td>
                <td>{this.props.tAllEmployees.employeefirstname}</td>
                <td>{this.props.tAllEmployees.employeemobilenumber}</td>
            </tr>
        )
    }
}

export default AllEmployeesTable;