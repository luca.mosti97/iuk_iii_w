import React from "react";
import {Link} from "react-router-dom";

import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import history from "../historyDef";

import PersonIcon from "../icons/feather/user-plus.svg";
import CancelIcon from "../icons/feather/x-circle.svg";

class NewEmployee extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstname: "",
            lastname: "",
            phonenumber: ""
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.addEmployee = this.addEmployee.bind(this);
    }


    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState( {
            [name]: value
        })
    }

    addEmployee(){
        let body = {
            firstname: this.state.firstname,
            lastname: this.state.lastname,
            phonenumber: this.state.phonenumber
        }

        fetch('/api/newEmployee',{
            method: 'post',
            body: JSON.stringify(body),
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            }

        }).then(response => {
            if (response.status !== 201) {
                alert("Bitte fülle alle Felder aus!");
            } else {
                alert("Mitarbeiter wurde erfasst!")
                history.push("/Employee")
                history.go(1);
            }
        });
    }

    render() {
        return(
            <main role={"main"} className={"ml-sm-auto px-md-4"}>
                <h2>Neuen Mitarbeiter hinzufügen</h2>
                <Form>
                    <Form.Group>
                        <Form.Label htmlFor={"form-firstname"} className={"form-labels"}>Vorname</Form.Label>
                        <Form.Control id={"form-firstname"} name={"firstname"} type={"text"} placeholder={"Vorname"} onChange={this.handleInputChange} />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor={"form-lastname"} className={"form-labels"}>Nachname</Form.Label>
                        <Form.Control id={"form-lastname"} name={"lastname"} type={"text"} placeholder={"Nachname"} onChange={this.handleInputChange}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor={"form-phonenumber"} className={"form-labels"}>Telefonnummer</Form.Label>
                        <Form.Control id={"form-phonenumber"} name={"phonenumber"} type={"text"} placeholder={"Telefonnummer"} onChange={this.handleInputChange}/>
                    </Form.Group>
                    <Row className={"form-button-bar-below"}>
                        <Col xs={12} sm={6}>
                            <Button className={"newVisitorButton"} variant={"info"} onClick={() => {this.addEmployee()}}>
                                <img alt={"Person icon"} src={PersonIcon} id={"icons"} />&nbsp;Neuer&nbsp;Mitarbeiter
                            </Button>
                        </Col>
                        <Col xs={12} sm={6}>
                            <Button as={Link} to={"/Employee"} className={"button-float-right button-float-none-very-small-screen cancel-button"} variant={"secondary"}>
                                <img alt={"Cancel icon"} src={CancelIcon} id={"icons"} />&nbsp;Cancel
                            </Button>
                        </Col>
                    </Row>
                </Form>
            </main>
        )
    }
}


export default NewEmployee;
