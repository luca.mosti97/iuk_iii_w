import React from "react";
import {Link} from "react-router-dom";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Table from "react-bootstrap/Table";
import Spinner from "react-bootstrap/Spinner";

import LastVisitorsTable from "../visitors/LastVisitorsTable";
import ActiveVisitorsTable from "../visitors/ActiveVisitorsTable";

import PersonIcon from "../icons/feather/user-plus.svg";
import EditIcon from "../icons/feather/edit.svg";

import "./main.css";

class Homescreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visitors: [],
            lastDeparted: [],
            isLoadingActive: true,
            isLoadingDeparted: true
        };
    }

    componentDidMount() {
        fetch("/api/activeVisits")
            .then((response => {
                return response.json();
            }))
            .then(data => {
                this.setState({
                    visitors: data,
                    isLoadingActive: false
                })
            });
        fetch("/api/lastDeparted")
            .then((response => {
                return response.json();
            }))
            .then(data => {
                this.setState({
                    lastDeparted: data,
                    isLoadingDeparted: false
                })
            });
    }

    render() {
        return(
            <main role={"main"} className={"ml-sm-auto px-md-4"} id={"page-content-wrapper"}>
                <Container fluid>
                    <Row>
                        <Col xs={0} sm={1}></Col>
                        <Col xs={12} sm={3}>
                            <Button as={Link} to={"/newVisitor"} className={"navigation-buttons"} variant={"info"}>
                                <img alt={"Person icon"} src={PersonIcon} className={"icons"} />&nbsp;Neuer Besucher
                            </Button>
                        </Col>
                        <Col xs={0} sm={2} md={4}></Col>
                        <Col xs={12} sm={3}>
                            <Button as={Link} to={"/newCompany"} className={"navigation-buttons"} variant={"info"}>
                                <img alt={"Edit icon"} src={EditIcon} className={"icons"} />&nbsp;Neue Firma
                            </Button>
                        </Col>
                        <Col xs={0} sm={1}></Col>
                    </Row>
                </Container>
                <hr />
                    <Container fluid>
                        <Row>
                            <Col xs={12} md={7}>
                                <h2>Anwesende Besucher</h2>
                            </Col>
                            <Col xs={12} md={5}>
                                <Button as={Link} to={"/newVisit"} className={"new-visit-button"}
                                        variant={"success"}>
                                    <img alt={"Person icon"} src={PersonIcon} className={"icons"}/>&nbsp;Neuer
                                    Besuch
                                </Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                {
                                    this.state.isLoadingActive ?
                                        <Container>
                                            <div className={"spinnerContainer"}>
                                                <Spinner animation={"border"} role={"status"}
                                                         aria-label={"Loading animation"}/>
                                            </div>
                                        </Container>
                                        :
                                        <Table striped hover responsive>
                                            <thead className={"thead-dark"}>
                                            <tr>
                                                <th>Datum</th>
                                                <th>Ankunft&nbsp;&#9650;</th>
                                                <th>Firma</th>
                                                <th>Name</th>
                                                <th>Vorname</th>
                                                <th>Zust&auml;ndiger Mitarbeiter</th>
                                                <th>Editieren</th>
                                                <th>Abreise</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {
                                                this.state.visitors.length > 0 ?
                                                    this.state.visitors.map(function (tActiveVisits) {
                                                        return (<ActiveVisitorsTable key={tActiveVisits.visitid}
                                                                                     tActiveVisits={tActiveVisits}/>)
                                                    })
                                                    :
                                                    <tr>
                                                        <td>Keine Besucher vorhanden</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                            }
                                            </tbody>
                                        </Table>
                                }
                            </Col>
                        </Row>
                    </Container>
                <hr />
                    <Container fluid>
                        <Row>
                            <Col>
                                <h2>Letzte Besucher</h2>
                                {
                                    this.state.isLoadingDeparted ?
                                        <Container>
                                            <div className={"spinnerContainer"}>
                                                <Spinner animation={"border"} role={"status"}
                                                         aria-label={"Loading animation"}/>
                                            </div>
                                        </Container>
                                        :
                                        <Table striped hover responsive>
                                            <thead className={"thead-dark"}>
                                            <tr>
                                                <th>Datum</th>
                                                <th>Abreise&nbsp;&#9650;</th>
                                                <th>Firma</th>
                                                <th>Name</th>
                                                <th>Vorname</th>
                                                <th>Zust&auml;ndiger Mitarbeiter</th>
                                                <th>Reaktivieren</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {
                                                this.state.lastDeparted.length > 0 ?
                                                    this.state.lastDeparted.map(function (tLastDeparted) {
                                                        return (<LastVisitorsTable key={tLastDeparted.visitid}
                                                                                   tLastDeparted={tLastDeparted}/>)
                                                    })
                                                    :
                                                    <tr>
                                                        <td>Keine k&uuml;rzlich abgereisten Besucher vorhanden</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                            }
                                            </tbody>
                                        </Table>
                                }
                            </Col>
                        </Row>
                    </Container>
            </main>
        )
    }
}


export default Homescreen;
