import React from "react";
import {Link} from "react-router-dom";

import Navbar from "react-bootstrap/Navbar";
import NavbarBrand from "react-bootstrap/NavbarBrand";
import Nav from "react-bootstrap/Nav";

class NavigationBar extends React.Component {
    render() {
        return(
            <Navbar className={"navbar-dark bg-dark flex-md-nowrap p-0 shadow"} >
                <Nav>
                    <NavbarBrand as={Link} to={"/home"} className={"col-md-3 col-lg-2 mr-0 px-3"}>Visitor Software</NavbarBrand>
                </Nav>
            </Navbar>
        )
    }
}

export default NavigationBar;