import React from "react";
import {Link} from "react-router-dom";

import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import Col from "react-bootstrap/Col";

import HomeIcon from "../icons/feather/home.svg";
import VisitorIcon from "../icons/feather/user.svg";
import ReportIcon from "../icons/feather/bar-chart.svg";
import WarningIcon from "../icons/feather/alert-triangle.svg";
import ScreenIcon from "../icons/feather/cast.svg";

import "./Sidebar.css";


class Sidebar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.toggleNavbar = this.toggleNavbar.bind(this);
    }

    /**
     * as bootstrap doesnt f-in work, and do as it's told, this function is needed
     * it simulates a click on the navbar-toggler to collapse the navbar, when the window is smaller than 1200px
     * the only other way would be to work with Nav.Link items directly
     * a Nav.Link rerenders the whole site, even when "withRouter" is implemented
     */
    toggleNavbar() {
        let width = window.innerWidth;
        if (width < 1200) {
            document.getElementById("sidebar-navbar-toggler").click();
        }
    }

    render() {
        return(
            <Col xs={12} xl={2} className={"p-0 sidebar-container"}>
                <Navbar variant={"light"} bg={"light"} expand={"xl"} className={"sidebar-navbar"} collapseOnSelect={true}>
                    <Navbar.Toggle id={"sidebar-navbar-toggler"} aria-controls={"sidebar-navbar-nav"} />
                    <Navbar.Collapse id={"sidebar-navbar-nav"}>
                        <Nav className={"mr-auto flex-column sidebar"} id={"sidebarMenu"}>
                            <Link to={"/home"} className={"nav-link"} onClick={this.toggleNavbar}>
                                <span><img alt={"Home Icon"} src={HomeIcon} className={"icons"} /> </span>&nbsp;Dashboard
                            </Link>
                            <Link to={"/visitors-overview"} className={"nav-link"} onClick={this.toggleNavbar}>
                                <span><img alt={"Visitor Icon"} src={VisitorIcon} className={"icons"} /> </span>&nbsp;Besucher
                            </Link>
                            <Link to={"/Employee"} className={"nav-link"} onClick={this.toggleNavbar}>
                                <span><img alt={"Employee Icon"} src={VisitorIcon} className={"icons"} /> </span>&nbsp;Mitarbeiter
                            </Link>
                            <Link to={"/reports"} className={"nav-link"} onClick={this.toggleNavbar}>
                                <span><img alt={"Report Icon"} src={ReportIcon} className={"icons"} /> </span>&nbsp;Reports
                            </Link>
                            <Link to={"/welcomeInput"} className={"nav-link"} onClick={this.toggleNavbar}>
                                <span><img alt={"Screen Icon"} src={ScreenIcon} className={"icons"} /> </span>&nbsp;Welcome
                            </Link>
                            <Link to={"/emergency"} className={"nav-link"} id={"emergency-link"} onClick={this.toggleNavbar}>
                                <span><img alt={"Warning Icon"} src={WarningIcon} className={"icons"} /> </span>&nbsp;Notfallmodus
                            </Link>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </Col>
        )
    }
}

export default Sidebar;