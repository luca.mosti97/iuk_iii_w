import React from "react";
import {Link} from "react-router-dom";
import Select from "react-select";

import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import history from "../historyDef";

import PersonIcon from "../icons/feather/user-plus.svg";
import CancelIcon from "../icons/feather/x-circle.svg";
import Container from "react-bootstrap/Container";
import Spinner from "react-bootstrap/Spinner";


class NewCompany extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            countries: [],
            countryid: 215, //Switzerland
            country: "",
            company: "",
            street: "",
            postalcode: "",
            countryoption: "",
            selectOptionsCountries: [],
            isLoadingCountries: true
        };
        this.getOptionsCountry = this.getOptionsCountry.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleInputChangeSelectCountry = this.handleInputChangeSelectCountry.bind(this);
        this.getDefaultCountryOption = this.getDefaultCountryOption.bind(this);
        this.addCompany = this.addCompany.bind(this);
    }

    /**
     * Initial Fetch Method
     */
    componentDidMount() {
        fetch("/api/allCountries")
            .then((response => {
                return response.json();
            }))
            .then(data => {
                this.setState({
                    countries: data.map(c => ({
                        "countryid": c.countryid,
                        "country": c.country
                    })),
                    isLoadingCountries: false
                })
            })
            .then(this.getOptionsCountry)
            .then(this.getDefaultCountryOption)
        ;
    }

    /**
     * For loading the options for Country react-select component
     * Gets called by {Link}
     * @returns {Promise<void>}
     */
    async getOptionsCountry() {
        let list = this.state.countries;
        let options = [];
        for (let i=0; i < list.length; i++) {
            options.push({
                "value": list[i].countryid,
                "label": list[i].country
            })
        }
        this.setState({
            selectOptionsCountries: options
        })
    }

    /**
     * Normal Handler for fields
     * @param event
     */
    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState( {
            [name]: value
        })
    }

    /**
     * Sets the option in Country select, value is in option.value, but state needs whole object
     * @param option    the new option from react-select component
     */
    handleInputChangeSelectCountry(option) {
        this.setState( {
            countryoption: option
        })
    }

    /**
     * Sets the option to be the first country
     */
    async getDefaultCountryOption() {
        let list = this.state.selectOptionsCountries;
        for (let i = 0; i < list.length; i++) {
            if (list[i].value === this.state.countryid) {
                this.setState({
                    countryoption: list[i]
                })
            }
        }
    }

    /**
     * POST method
     */
    addCompany(){
        let body = {
            company: this.state.company,
            street: this.state.street,
            city: this.state.city,
            postalcode: this.state.postalcode,
            countryid: this.state.countryoption.value
        }

        fetch('/api/newCompany',{
            method: 'post',
            body: JSON.stringify(body),
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            }

        }).then(response => {
            if (response.status !== 201) {
                alert("Bitte fülle alle Felder aus!");
            } else {
                //alert("Neue Firma wurde erfasst!")
                history.push("/home")
                history.go(1);
            }
        });
    }

    render() {
        return(
            <main role={"main"} className={"ml-sm-auto px-md-4"}>
                <h2>Neue Firma hinzuf&uuml;gen</h2>
                <Form>
                    <Form.Group>
                        <Form.Label htmlFor={"form-company"} className={"form-labels"}>Firmenname</Form.Label>
                        <Form.Control id={"form-company"} name={"company"} type={"text"} placeholder={"Firmenname"} onChange={this.handleInputChange}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor={"form-street"} className={"form-labels"}>Strasse</Form.Label>
                        <Form.Control id={"form-street"} name={"street"} type={"text"} placeholder={"Strasse"} onChange={this.handleInputChange}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor={"form-postalcode"} className={"form-labels"}>PLZ</Form.Label>
                        <Form.Control id={"form-postalcode"} name={"postalcode"} type={"text"} placeholder={"PLZ"} onChange={this.handleInputChange}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor={"form-city"} className={"form-labels"}>Ort</Form.Label>
                        <Form.Control id={"form-city"} name={"city"} type={"text"} placeholder={"Ort"} onChange={this.handleInputChange}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor={"form-select-countryid"} className={"form-labels"}>Land</Form.Label>
                        {
                            this.state.isLoadingCountries ?
                                <Container>
                                    <div className={"spinnerContainer"}>
                                        <Spinner animation={"border"} role={"status"}
                                                 aria-label={"Loading animation"}/>
                                    </div>
                                </Container>
                                :
                                <Select
                                    id={"form-select-countryid"}
                                    name={"countryid"}
                                    options={this.state.selectOptionsCountries}
                                    onChange={this.handleInputChangeSelectCountry}
                                    value={this.state.countryoption}
                                    menuPlacement={"auto"}
                                    isSearchable={true}
                                    selectedValue={this.state.selectOptionsCountries[this.state.countryid]}
                                />
                        }
                    </Form.Group>
                    <Row className={"form-button-bar-below"}>
                        <Col>
                            <Button className={"newVisitorButton"} variant={"info"}
                            onClick={() => {this.addCompany()}}>
                            <img alt={"Person icon"} src={PersonIcon} className={"icons"} />&nbsp;Neue&nbsp;Firma
                            </Button>
                        </Col>
                        <Col>
                            <Button as={Link} to={"/home"} className={"button-float-right button-float-none-very-small-screen cancel-button"} variant={"secondary"}>
                                <img alt={"Cancel icon"} src={CancelIcon} className={"icons"} />&nbsp;Cancel
                            </Button>
                        </Col>
                    </Row>
                </Form>
            </main>
        )
    }
}


export default NewCompany;
